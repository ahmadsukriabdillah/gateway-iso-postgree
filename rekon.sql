-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 18, 2017 at 05:20 PM
-- Server version: 5.1.44
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rekon`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_biller`
--

CREATE TABLE IF NOT EXISTS `tb_biller` (
  `kd_biller` int(11) NOT NULL,
  `nm_biller` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_biller`
--


-- --------------------------------------------------------

--
-- Table structure for table `tb_ca`
--

CREATE TABLE IF NOT EXISTS `tb_ca` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `kd_ca` varchar(255) NOT NULL,
  `nm_ca` varchar(255) NOT NULL,
  `rek_ca` varchar(255) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `tb_ca`
--


-- --------------------------------------------------------

--
-- Table structure for table `tb_cab`
--

CREATE TABLE IF NOT EXISTS `tb_cab` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `kd_cab` varchar(255) NOT NULL,
  `nm_cab` varchar(255) NOT NULL,
  `rek_cab` varchar(255) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=152 ;

--
-- Dumping data for table `tb_cab`
--


-- --------------------------------------------------------

--
-- Table structure for table `tb_dtbkp`
--

CREATE TABLE IF NOT EXISTS `tb_dtbkp` (
  `vldat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `calculate` int(11) NOT NULL,
  `TRDES1` varchar(255) NOT NULL,
  `ACNOT` varchar(255) NOT NULL,
  `ACNOC` varchar(255) NOT NULL,
  `TRDES2` varchar(255) NOT NULL,
  `AMTR` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_dtbkp`
--


-- --------------------------------------------------------

--
-- Table structure for table `tb_fee`
--

CREATE TABLE IF NOT EXISTS `tb_fee` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `kd_prd` varchar(255) NOT NULL,
  `fee_bkp` int(11) NOT NULL,
  `fee_swt` int(11) NOT NULL,
  `fee_ca` int(11) NOT NULL,
  `fee_mrc` int(11) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tb_fee`
--


-- --------------------------------------------------------

--
-- Table structure for table `tb_gspprepaid`
--

CREATE TABLE IF NOT EXISTS `tb_gspprepaid` (
  `NO` varchar(255) NOT NULL,
  `MITRA` varchar(255) NOT NULL,
  `PPID` varchar(255) NOT NULL,
  `MSN` varchar(255) NOT NULL,
  `IDPEL` varchar(255) NOT NULL,
  `NAMA_PELANGGAN` varchar(255) NOT NULL,
  `MLPO_REFNUM` varchar(255) NOT NULL,
  `SWITCH_REFNUM` varchar(255) NOT NULL,
  `RP_TAGIHAN` varchar(255) NOT NULL,
  `RP_ADMIN` varchar(255) NOT NULL,
  `NO_TOKEN` varchar(255) NOT NULL,
  `TANGGAL_TRANSAKSI` varchar(255) NOT NULL,
  `MERCHANT` varchar(255) NOT NULL,
  `kd_transaksi` int(11) NOT NULL DEFAULT '24',
  `hist_date` bigint(20) NOT NULL,
  `kd_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_gspprepaid`
--


-- --------------------------------------------------------

--
-- Table structure for table `tb_gspprepaid_temp`
--

CREATE TABLE IF NOT EXISTS `tb_gspprepaid_temp` (
  `NO` varchar(255) NOT NULL,
  `MITRA` varchar(255) NOT NULL,
  `PPID` varchar(255) NOT NULL,
  `MSN` varchar(255) NOT NULL,
  `IDPEL` varchar(255) NOT NULL,
  `NAMA_PELANGGAN` varchar(255) NOT NULL,
  `MLPO_REFNUM` varchar(255) NOT NULL,
  `SWITCH_REFNUM` varchar(255) NOT NULL,
  `RP_TAGIHAN` varchar(255) NOT NULL,
  `RP_ADMIN` varchar(255) NOT NULL,
  `NO_TOKEN` varchar(255) NOT NULL,
  `TANGGAL_TRANSAKSI` varchar(255) NOT NULL,
  `MERCHANT` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_gspprepaid_temp`
--


-- --------------------------------------------------------

--
-- Table structure for table `tb_jnrek`
--

CREATE TABLE IF NOT EXISTS `tb_jnrek` (
  `jns_rek` int(11) NOT NULL,
  `keterangan` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_jnrek`
--


-- --------------------------------------------------------

--
-- Table structure for table `tb_mapping`
--

CREATE TABLE IF NOT EXISTS `tb_mapping` (
  `kd_transaksi` int(11) NOT NULL,
  `nm_transaksi` varchar(255) NOT NULL,
  `kat_transaksi` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_mapping`
--


-- --------------------------------------------------------

--
-- Table structure for table `tb_mrc`
--

CREATE TABLE IF NOT EXISTS `tb_mrc` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `mid` varchar(255) NOT NULL,
  `tid` varchar(255) NOT NULL,
  `nm_mrc` varchar(255) NOT NULL,
  `nm_rek_mrc` varchar(255) NOT NULL,
  `rek_mrc` varchar(255) NOT NULL,
  `jns_rek` varchar(255) NOT NULL,
  `card_num` varchar(255) NOT NULL,
  `rek_fee_mrc` varchar(255) NOT NULL,
  `kd_ca` int(11) NOT NULL,
  `kd_cab` varchar(10) NOT NULL,
  `tgl_request` date NOT NULL,
  `tgl_pasang` date NOT NULL,
  `tgl_tutup` date NOT NULL,
  `alasan_tutup` text NOT NULL,
  `vendor` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2485 ;

--
-- Dumping data for table `tb_mrc`
--


-- --------------------------------------------------------

--
-- Table structure for table `tb_mrc01`
--

CREATE TABLE IF NOT EXISTS `tb_mrc01` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `mid` varchar(255) NOT NULL,
  `tid` varchar(255) NOT NULL,
  `nm_mrc` varchar(255) NOT NULL,
  `nm_rek_mrc` varchar(255) NOT NULL,
  `rek_mrc` varchar(255) NOT NULL,
  `jns_rek` varchar(255) NOT NULL,
  `card_num` varchar(255) NOT NULL,
  `rek_fee_mrc` varchar(255) NOT NULL,
  `kd_ca` int(11) NOT NULL,
  `kd_cab` varchar(10) NOT NULL,
  `vendor` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=441 ;

--
-- Dumping data for table `tb_mrc01`
--


-- --------------------------------------------------------

--
-- Table structure for table `tb_mrc08032013`
--

CREATE TABLE IF NOT EXISTS `tb_mrc08032013` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `mid` varchar(255) NOT NULL,
  `tid` varchar(255) NOT NULL,
  `nm_mrc` varchar(255) NOT NULL,
  `nm_rek_mrc` varchar(255) NOT NULL,
  `rek_mrc` varchar(255) NOT NULL,
  `jns_rek` varchar(255) NOT NULL,
  `card_num` varchar(255) NOT NULL,
  `rek_fee_mrc` varchar(255) NOT NULL,
  `kd_ca` int(11) NOT NULL,
  `kd_cab` varchar(10) NOT NULL,
  `tgl_request` date NOT NULL,
  `tgl_pasang` date NOT NULL,
  `tgl_tutup` date NOT NULL,
  `alasan_tutup` text NOT NULL,
  `vendor` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=571 ;

--
-- Dumping data for table `tb_mrc08032013`
--


-- --------------------------------------------------------

--
-- Table structure for table `tb_mrc_kartuku_aktif`
--

CREATE TABLE IF NOT EXISTS `tb_mrc_kartuku_aktif` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `mid` varchar(255) NOT NULL,
  `tid` varchar(255) NOT NULL,
  `nm_mrc` varchar(255) NOT NULL,
  `nm_rek_mrc` varchar(255) NOT NULL,
  `rek_mrc` varchar(255) NOT NULL,
  `jns_rek` varchar(255) NOT NULL,
  `card_num` varchar(255) NOT NULL,
  `rek_fee_mrc` varchar(255) NOT NULL,
  `kd_ca` int(11) NOT NULL,
  `kd_cab` varchar(10) NOT NULL,
  `tgl_request` date NOT NULL,
  `tgl_pasang` date NOT NULL,
  `tgl_tutup` date NOT NULL,
  `alasan_tutup` text NOT NULL,
  `vendor` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=652 ;

--
-- Dumping data for table `tb_mrc_kartuku_aktif`
--


-- --------------------------------------------------------

--
-- Table structure for table `tb_mrc_kartuku_pasif`
--

CREATE TABLE IF NOT EXISTS `tb_mrc_kartuku_pasif` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `mid` varchar(255) NOT NULL,
  `tid` varchar(255) NOT NULL,
  `nm_mrc` varchar(255) NOT NULL,
  `nm_rek_mrc` varchar(255) NOT NULL,
  `rek_mrc` varchar(255) NOT NULL,
  `jns_rek` varchar(255) NOT NULL,
  `card_num` varchar(255) NOT NULL,
  `rek_fee_mrc` varchar(255) NOT NULL,
  `kd_ca` int(11) NOT NULL,
  `kd_cab` varchar(10) NOT NULL,
  `tgl_request` date NOT NULL,
  `tgl_pasang` date NOT NULL,
  `tgl_tutup` date NOT NULL,
  `alasan_tutup` text NOT NULL,
  `vendor` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=258 ;

--
-- Dumping data for table `tb_mrc_kartuku_pasif`
--


-- --------------------------------------------------------

--
-- Table structure for table `tb_mrc_primavista_aktif`
--

CREATE TABLE IF NOT EXISTS `tb_mrc_primavista_aktif` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `mid` varchar(255) NOT NULL,
  `tid` varchar(255) NOT NULL,
  `nm_mrc` varchar(255) NOT NULL,
  `nm_rek_mrc` varchar(255) NOT NULL,
  `rek_mrc` varchar(255) NOT NULL,
  `jns_rek` varchar(255) NOT NULL,
  `card_num` varchar(255) NOT NULL,
  `rek_fee_mrc` varchar(255) NOT NULL,
  `kd_ca` int(11) NOT NULL,
  `kd_cab` varchar(10) NOT NULL,
  `tgl_request` date NOT NULL,
  `tgl_pasang` date NOT NULL,
  `tgl_tutup` date NOT NULL,
  `alasan_tutup` text NOT NULL,
  `vendor` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=450 ;

--
-- Dumping data for table `tb_mrc_primavista_aktif`
--


-- --------------------------------------------------------

--
-- Table structure for table `tb_mrc_primavista_pasif`
--

CREATE TABLE IF NOT EXISTS `tb_mrc_primavista_pasif` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `mid` varchar(255) NOT NULL,
  `tid` varchar(255) NOT NULL,
  `nm_mrc` varchar(255) NOT NULL,
  `nm_rek_mrc` varchar(255) NOT NULL,
  `rek_mrc` varchar(255) NOT NULL,
  `jns_rek` varchar(255) NOT NULL,
  `card_num` varchar(255) NOT NULL,
  `rek_fee_mrc` varchar(255) NOT NULL,
  `kd_ca` int(11) NOT NULL,
  `kd_cab` varchar(10) NOT NULL,
  `tgl_request` date NOT NULL,
  `tgl_pasang` date NOT NULL,
  `tgl_tutup` date NOT NULL,
  `alasan_tutup` text NOT NULL,
  `vendor` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=678 ;

--
-- Dumping data for table `tb_mrc_primavista_pasif`
--


-- --------------------------------------------------------

--
-- Table structure for table `tb_mrc_tmp`
--

CREATE TABLE IF NOT EXISTS `tb_mrc_tmp` (
  `tgl_request` varchar(255) NOT NULL,
  `tgl_pasang` varchar(255) NOT NULL,
  `mid` varchar(255) NOT NULL,
  `tid` varchar(255) NOT NULL,
  `payment` varchar(255) NOT NULL,
  `ob` varchar(255) NOT NULL,
  `ft` varchar(255) NOT NULL,
  `withdrawal` varchar(255) NOT NULL,
  `topup` varchar(255) NOT NULL,
  `setor` varchar(255) NOT NULL,
  `lim_withdrawal` varchar(255) NOT NULL,
  `lim_setor` varchar(255) NOT NULL,
  `kd_jns_mrc` varchar(255) NOT NULL,
  `jns_mrc` varchar(255) NOT NULL,
  `nm_mrc` varchar(255) NOT NULL,
  `addr_mrc` varchar(255) NOT NULL,
  `telp_mrc` varchar(255) NOT NULL,
  `nm_pic_mrc` varchar(255) NOT NULL,
  `telp_pic_mrc` varchar(255) NOT NULL,
  `rek_fee_mrc` varchar(255) NOT NULL,
  `rek_mrc` varchar(255) NOT NULL,
  `jns_rek` varchar(255) NOT NULL,
  `card_num` varchar(255) NOT NULL,
  `nm_rek_mrc` varchar(255) NOT NULL,
  `status_ksg` varchar(255) DEFAULT NULL,
  `kd_ca` varchar(255) NOT NULL,
  `nm_ca` varchar(255) NOT NULL,
  `kd_cab` varchar(255) NOT NULL,
  `nm_cab` varchar(255) NOT NULL,
  `si` varchar(255) NOT NULL,
  `pks` varchar(255) NOT NULL,
  `form_pembukaan` varchar(255) NOT NULL,
  `spanduk` varchar(255) NOT NULL,
  `tgl_tutup` varchar(255) NOT NULL,
  `alasan_tutup` text NOT NULL,
  `vendor` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_mrc_tmp`
--


-- --------------------------------------------------------

--
-- Table structure for table `tb_mrc_tmp01`
--

CREATE TABLE IF NOT EXISTS `tb_mrc_tmp01` (
  `mid` varchar(255) NOT NULL,
  `tid` varchar(255) NOT NULL,
  `nm_mrc` varchar(255) NOT NULL,
  `nm_rek_mrc` varchar(255) NOT NULL,
  `rek_mrc` varchar(255) NOT NULL,
  `jns_rek` varchar(255) NOT NULL,
  `card_num` varchar(255) NOT NULL,
  `rek_fee_mrc` varchar(255) NOT NULL,
  `kd_ca` int(11) NOT NULL,
  `kd_cab` varchar(10) NOT NULL,
  `vendor` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_mrc_tmp01`
--


-- --------------------------------------------------------

--
-- Table structure for table `tb_prd`
--

CREATE TABLE IF NOT EXISTS `tb_prd` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `kd_prd` varchar(255) NOT NULL,
  `nm_prd` varchar(255) NOT NULL,
  `denom` int(11) NOT NULL,
  `kd_swt` varchar(255) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `tb_prd`
--


-- --------------------------------------------------------

--
-- Table structure for table `tb_prefix`
--

CREATE TABLE IF NOT EXISTS `tb_prefix` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `kd_prd` varchar(255) NOT NULL,
  `prefix` varchar(255) NOT NULL,
  `kd_biller` int(11) NOT NULL,
  `kd_swt` varchar(255) NOT NULL,
  `denom` int(11) NOT NULL,
  `fee_bkp` int(11) NOT NULL,
  `fee_ca` int(11) NOT NULL,
  `fee_mrc` int(11) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tb_prefix`
--


-- --------------------------------------------------------

--
-- Table structure for table `tb_status`
--

CREATE TABLE IF NOT EXISTS `tb_status` (
  `no` int(11) DEFAULT NULL,
  `kd_status` varchar(255) NOT NULL,
  `nm_status` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_status`
--


-- --------------------------------------------------------

--
-- Table structure for table `tb_statusmrc`
--

CREATE TABLE IF NOT EXISTS `tb_statusmrc` (
  `kd_status` int(11) NOT NULL,
  `desc` varchar(255) NOT NULL,
  PRIMARY KEY (`kd_status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_statusmrc`
--


-- --------------------------------------------------------

--
-- Table structure for table `tb_swt`
--

CREATE TABLE IF NOT EXISTS `tb_swt` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `kd_swt` varchar(255) NOT NULL,
  `nm_swt` varchar(255) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `tb_swt`
--


-- --------------------------------------------------------

--
-- Table structure for table `tb_trxatmb_temp`
--

CREATE TABLE IF NOT EXISTS `tb_trxatmb_temp` (
  `BTCOD` varchar(255) CHARACTER SET utf8 NOT NULL,
  `VLDAT` varchar(255) CHARACTER SET utf8 NOT NULL,
  `INTIM` varchar(255) CHARACTER SET utf8 NOT NULL,
  `COMPUTE_0004` varchar(255) CHARACTER SET utf8 NOT NULL,
  `TRDES1` varchar(255) CHARACTER SET utf8 NOT NULL,
  `ACNOT` varchar(255) CHARACTER SET utf8 NOT NULL,
  `ACNOC` varchar(255) CHARACTER SET utf8 NOT NULL,
  `TRDES2` varchar(255) CHARACTER SET utf8 NOT NULL,
  `AMTR` varchar(255) CHARACTER SET utf8 NOT NULL,
  `TRST` text CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_trxatmb_temp`
--


-- --------------------------------------------------------

--
-- Table structure for table `tb_trxbkp`
--

CREATE TABLE IF NOT EXISTS `tb_trxbkp` (
  `BTCOD` varchar(255) CHARACTER SET utf8 NOT NULL,
  `VLDAT` varchar(255) CHARACTER SET utf8 NOT NULL,
  `INTIM` varchar(255) CHARACTER SET utf8 NOT NULL,
  `COMPUTE_0004` varchar(255) CHARACTER SET utf8 NOT NULL,
  `TRDES1` varchar(255) CHARACTER SET utf8 NOT NULL,
  `ACNOT` varchar(255) CHARACTER SET utf8 NOT NULL,
  `ACNOC` varchar(255) CHARACTER SET utf8 NOT NULL,
  `TRDES2` varchar(255) CHARACTER SET utf8 NOT NULL,
  `AMTR` varchar(255) CHARACTER SET utf8 NOT NULL,
  `TRST` text CHARACTER SET utf8 NOT NULL,
  `kd_transaksi` int(11) NOT NULL,
  `hist_date` bigint(20) NOT NULL,
  `kd_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_trxbkp`
--


-- --------------------------------------------------------

--
-- Table structure for table `tb_trxprima_temp`
--

CREATE TABLE IF NOT EXISTS `tb_trxprima_temp` (
  `BTCOD` varchar(255) NOT NULL,
  `VLDAT` varchar(255) NOT NULL,
  `INTIM` varchar(255) NOT NULL,
  `COMPUTE_0004` varchar(255) NOT NULL,
  `TRDES1` varchar(255) NOT NULL,
  `ACNOT` varchar(255) NOT NULL,
  `ACNOC` varchar(255) NOT NULL,
  `TRDES2` varchar(255) NOT NULL,
  `AMTR` varchar(255) NOT NULL,
  `TRST` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_trxprima_temp`
--


-- --------------------------------------------------------

--
-- Table structure for table `tb_trxscb`
--

CREATE TABLE IF NOT EXISTS `tb_trxscb` (
  `tgl` date NOT NULL,
  `tgl_wkt` time NOT NULL,
  `unix_timestamp` varchar(255) NOT NULL,
  `kd_prd` varchar(255) NOT NULL,
  `tid` int(11) NOT NULL,
  `nominal` int(11) NOT NULL,
  `denom` int(11) NOT NULL,
  `no_tujuan` varchar(255) NOT NULL,
  `no_kartu` varchar(255) NOT NULL,
  `rek_asal` varchar(255) NOT NULL,
  `rek_tujuan` varchar(255) NOT NULL,
  `kd_swt` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_trxscb`
--


-- --------------------------------------------------------

--
-- Table structure for table `tb_trxscs`
--

CREATE TABLE IF NOT EXISTS `tb_trxscs` (
  `tgl` date NOT NULL,
  `tgl_wkt` time NOT NULL,
  `unix_timestamp` varchar(255) NOT NULL,
  `kd_prd` varchar(255) NOT NULL,
  `tid` int(11) NOT NULL,
  `nominal` int(11) NOT NULL,
  `denom` int(11) NOT NULL,
  `no_tujuan` varchar(255) NOT NULL,
  `no_kartu` varchar(255) NOT NULL,
  `rek_asal` varchar(255) NOT NULL,
  `rek_tujuan` varchar(255) NOT NULL,
  `kd_swt` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_trxscs`
--


-- --------------------------------------------------------

--
-- Table structure for table `tb_trxswt`
--

CREATE TABLE IF NOT EXISTS `tb_trxswt` (
  `tgl` date NOT NULL,
  `tgl_wkt` time NOT NULL,
  `tid` varchar(255) NOT NULL,
  `terminalacc` varchar(255) NOT NULL,
  `lokasiterminal` varchar(255) NOT NULL,
  `bankterminal` varchar(255) NOT NULL,
  `nokartu` varchar(255) NOT NULL,
  `resi` varchar(255) NOT NULL,
  `cur1` varchar(255) NOT NULL,
  `nominal` varchar(255) NOT NULL,
  `cur2` varchar(255) NOT NULL,
  `idrnominal` varchar(255) NOT NULL,
  `bankasaldebet` varchar(255) NOT NULL,
  `rekeningasaldebet` varchar(255) NOT NULL,
  `namaasaldebet` varchar(255) NOT NULL,
  `banktujuan` varchar(255) NOT NULL,
  `rekeningtujuankredit` varchar(255) NOT NULL,
  `namatujuankredit` varchar(255) NOT NULL,
  `prodcode` varchar(255) NOT NULL,
  `prodcodename` varchar(255) NOT NULL,
  `merchantcode` varchar(255) NOT NULL,
  `merchantname` varchar(255) NOT NULL,
  `transactioncode` varchar(255) NOT NULL,
  `transactionname` varchar(255) NOT NULL,
  `switchnet` varchar(255) NOT NULL,
  `requestamount` varchar(255) NOT NULL,
  `completeamount` varchar(255) NOT NULL,
  `responsecode` varchar(255) NOT NULL,
  `response` varchar(255) NOT NULL,
  `coreresponse` varchar(255) NOT NULL,
  `coreresponsedesc` varchar(255) NOT NULL,
  `corenotifresponse` varchar(255) NOT NULL,
  `corenotifresponsedesc` varchar(255) NOT NULL,
  `sharednetresponse` varchar(255) NOT NULL,
  `sharednetresponsedesc` varchar(255) NOT NULL,
  `bc` varchar(255) NOT NULL,
  `stan` varchar(255) NOT NULL,
  `stan_rvs` varchar(255) NOT NULL,
  `stan_exception` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_trxswt`
--


-- --------------------------------------------------------

--
-- Table structure for table `tb_trxswt_tmp`
--

CREATE TABLE IF NOT EXISTS `tb_trxswt_tmp` (
  `tgl` varchar(255) NOT NULL,
  `tgl_wkt` varchar(255) NOT NULL,
  `tid` varchar(255) NOT NULL,
  `terminalacc` varchar(255) NOT NULL,
  `lokasiterminal` varchar(255) NOT NULL,
  `bankterminal` varchar(255) NOT NULL,
  `nokartu` varchar(255) NOT NULL,
  `resi` varchar(255) NOT NULL,
  `cur1` varchar(255) NOT NULL,
  `nominal` varchar(255) NOT NULL,
  `cur2` varchar(255) NOT NULL,
  `idrnominal` varchar(255) NOT NULL,
  `bankasaldebet` varchar(255) NOT NULL,
  `rekeningasaldebet` varchar(255) NOT NULL,
  `namaasaldebet` varchar(255) NOT NULL,
  `banktujuan` varchar(255) NOT NULL,
  `rekeningtujuankredit` varchar(255) NOT NULL,
  `namatujuankredit` varchar(255) NOT NULL,
  `prodcode` varchar(255) NOT NULL,
  `prodcodename` varchar(255) NOT NULL,
  `merchantcode` varchar(255) NOT NULL,
  `merchantname` varchar(255) NOT NULL,
  `transactioncode` varchar(255) NOT NULL,
  `transactionname` varchar(255) NOT NULL,
  `switchnet` varchar(255) NOT NULL,
  `requestamount` varchar(255) NOT NULL,
  `completeamount` varchar(255) NOT NULL,
  `responsecode` varchar(255) NOT NULL,
  `response` varchar(255) NOT NULL,
  `coreresponse` varchar(255) NOT NULL,
  `coreresponsedesc` varchar(255) NOT NULL,
  `corenotifresponse` varchar(255) NOT NULL,
  `corenotifresponsedesc` varchar(255) NOT NULL,
  `sharednetresponse` varchar(255) NOT NULL,
  `sharednetresponsedesc` varchar(255) NOT NULL,
  `bc` varchar(255) NOT NULL,
  `stan` varchar(255) NOT NULL,
  `stan_rvs` varchar(255) NOT NULL,
  `stan_exception` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_trxswt_tmp`
--


-- --------------------------------------------------------

--
-- Table structure for table `tb_trxvendor`
--

CREATE TABLE IF NOT EXISTS `tb_trxvendor` (
  `tgl` int(11) NOT NULL,
  `tgl_wkt` int(11) NOT NULL,
  `unix_timestamp` varchar(255) NOT NULL,
  `kd_prd` varchar(255) NOT NULL,
  `tid` int(11) NOT NULL,
  `nominal` int(11) NOT NULL,
  `denom` int(11) NOT NULL,
  `no_tujuan` varchar(255) NOT NULL,
  `no_kartu` varchar(255) NOT NULL,
  `rek_asal` varchar(255) NOT NULL,
  `rek_tujuan` varchar(255) NOT NULL,
  `kd_swt` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_trxvendor`
--


-- --------------------------------------------------------

--
-- Table structure for table `tb_vendor`
--

CREATE TABLE IF NOT EXISTS `tb_vendor` (
  `kd_vendor` varchar(255) NOT NULL,
  `nm_vendor` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_vendor`
--

