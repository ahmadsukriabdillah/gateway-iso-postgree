package main.java.com.trimindi.switching;

import org.jpos.q2.QBeanSupportMBean;

/**
 * Created by sx on 11/05/17.
 */
public interface QRestServerMBean extends QBeanSupportMBean {
    void setPort(int port);
    int getPort();
}

