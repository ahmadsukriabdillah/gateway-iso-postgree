package main.java.com.trimindi.switching.filter;

import main.java.com.trimindi.switching.services.partner.models.PartnerCredential;
import main.java.com.trimindi.switching.services.partner.services.PartnerCredentialService;
import main.java.com.trimindi.switching.utils.constanta.Constanta;
import main.java.com.trimindi.switching.utils.constanta.ResponseCode;
import org.apache.commons.codec.digest.DigestUtils;

import javax.annotation.Priority;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sx on 13/05/17.
 */

public class SecureEndpoint implements ContainerRequestFilter {

    @Context
    private HttpServletRequest sr;

    PartnerCredentialService partnerCredentialService;
    private static Map<String,String> whitelist;
    static {
        Map<String,String> w = new HashMap<>();
        w.put("partner/version","partner/version");
        whitelist = Collections.unmodifiableMap(w);
    }
    public SecureEndpoint() {
        partnerCredentialService = new PartnerCredentialService();
    }

    @Override
    public void filter(ContainerRequestContext req) throws IOException {
        Response SERVER_UNAUTORIZED = Response.status(200).entity(new ResponseCode("9997","Unauthorization request.")).build();


        String scheme = req.getUriInfo().getPath();
        if(whitelist.containsKey(scheme)){

        }else{
            String userid = req.getHeaderString("userid");
            String sign = req.getHeaderString("sign");
            String time = req.getHeaderString("time");
            if(userid == null || sign == null || time == null){
                req.abortWith(SERVER_UNAUTORIZED);
                return;
            }
            if(userid.isEmpty() || sign.isEmpty() || time.isEmpty()){
                req.abortWith(SERVER_UNAUTORIZED);
                return;
            }
//        String time = null;
//        try {
//            Date date = new SimpleDateFormat("yyyyMMddHHmmss").parse(req.getHeaderString("time"));
//            System.out.println("DATE FROM SIGN" + date.toString());
//            Date now = new Date(System.currentTimeMillis());
//            date.setTime(date.getTime() + 5* 60 * 1000);
//            System.out.println("DATE FROM Server" + now.toString());
//            if(date.before(now)){
//                throw new WebApplicationException(SERVER_UNAUTORIZED);
//            }else{
//                time = req.getHeaderString("time");
//            }
//        } catch (ParseException e) {
//            throw new WebApplicationException(SERVER_UNAUTORIZED);
//        }
            if(req.getHeaderString("accept").equals(MediaType.APPLICATION_JSON) || req.getHeaderString("accept").equals(MediaType.APPLICATION_XML)){
                PartnerCredential partnerCredential = partnerCredentialService.findByUserId(userid);
                System.out.println(partnerCredential);

                if(partnerCredential != null){
                    String pass = DigestUtils.sha1Hex(partnerCredential.getPartner_uid() + time +partnerCredential.getPartner_password());
                    if(sign.equals(pass)){
                        req.setProperty(Constanta.MAC , req.getHeaderString(Constanta.MAC));
                        req.setProperty(Constanta.IP_ADDRESS , sr.getRemoteAddr());
                        req.setProperty(Constanta.PRINCIPAL , partnerCredential);
                        req.setProperty(Constanta.LATITUDE,req.getHeaderString(Constanta.LATITUDE));
                        req.setProperty(Constanta.LONGITUDE,req.getHeaderString(Constanta.LONGITUDE));
                    }else {
                        req.abortWith(SERVER_UNAUTORIZED);
                        return;
                    }
                }else{
                    req.abortWith(SERVER_UNAUTORIZED);
                    return;
                }
            }else{
                req.abortWith(SERVER_UNAUTORIZED);
                return;
            }

        }
    }
}