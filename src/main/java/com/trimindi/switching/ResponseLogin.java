package main.java.com.trimindi.switching;

import main.java.com.trimindi.switching.services.partner.models.Partner;
import main.java.com.trimindi.switching.services.partner.models.PartnerDeposit;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by HP on 22/05/2017.
 */
@XmlRootElement(name = "response")
public class ResponseLogin {
    private boolean status;

    private Partner data;
    private PartnerDeposit desposit;

    public PartnerDeposit getDesposit() {
        return desposit;
    }

    public ResponseLogin setDesposit(PartnerDeposit desposit) {
        this.desposit = desposit;
        return this;
    }

    public ResponseLogin() {
    }

    public boolean isStatus() {
        return status;
    }

    public ResponseLogin setStatus(boolean status) {
        this.status = status;
        return this;
    }
    public Partner getData() {
        return data;
    }

    public ResponseLogin setData(Partner data) {
        this.data = data;
        return this;
    }
}
