package main.java.com.trimindi.switching.q2;

import org.jpos.q2.Q2;

import java.io.File;

/**
 * Created by sx on 12/05/17.
 */
public class Deploy {
    private static Deploy instace;
    private Q2 q2;
    private boolean started;

    public static synchronized void destroy(){
        instace.getQ2().shutdown();
        instace = null;
        System.out.println("Instance Destroyed");
    }

    /**
     * initiate singleton class
     * deploy Q2 with default deploy directory
     * @return
     */
    public static synchronized Deploy getInstance() {

        if (instace == null){
            instace = new Deploy();
            instace.setQ2(new Q2());
            instace.setStarted(false);
            System.out.println(instace.getQ2().toString());
        }

        return instace;
    }

    /**
     * initiate singleton class
     * deploy Q2 with custom deploy directory
     * @param deployDir
     * @return
     */
    public static synchronized Deploy getInstance(String deployDir) {

        if (instace == null){
            instace = new Deploy();
            instace.setQ2(new Q2(deployDir));
            instace.setStarted(false);
        }

        return instace;
    }

    /**
     * Start Q2 with default deploy directory
     */
    public synchronized void startQ2() {
        if (instace !=null && !instace.isStarted()) {
            instace.getQ2().start();
            instace.setStarted(true);
        }
    }

    /**
     * Stop Q2
     */
    public static synchronized void stopQ2() {
        if (instace != null && instace.isStarted()) {
            instace.getQ2().stop();
            instace.setStarted(false);
        }
    }

    /**
     * get Deploy Directory
     * @return absolute path
     */
    public static String getDeployDir() {
        if (instace != null && instace.getQ2() != null) {
            File deployDir = instace.getQ2().getDeployDir();
            return deployDir.getAbsolutePath();
        }
        return null;
    }

    /**
     * get Q2 start up time
     * @return start up time
     */
    public static Long getServerUpTime() {
        if (instace != null && instace.getQ2() != null) {
            return instace.getQ2().getUptime();
        }
        return 0L;
    }

    public static boolean isQ2Started(){
        if (instace != null) {
            return instace.isStarted();
        }
        return false;
    }

    /**
     * A private Constructor prevents any other class from instantiating.
     */
    private Deploy() { }

    public boolean isStarted() {
        return started;
    }

    private void setStarted(boolean started) {
        this.started = started;
    }

    private Q2 getQ2() {
        return q2;
    }

    private void setQ2(Q2 q2) {
        this.q2 = q2;
    }
}
