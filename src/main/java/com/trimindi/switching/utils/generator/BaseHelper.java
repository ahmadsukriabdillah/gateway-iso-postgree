package main.java.com.trimindi.switching.utils.generator;

import org.apache.commons.lang3.StringUtils;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * Created by sx on 13/05/17.
 */
public class BaseHelper {
    protected static final String PAN = "99501";
    protected static final String PAN_PREPAID = "99502";
    protected static final String PAN_POSTPAID = "99501";

    protected static final String PARTNER_ID = "4411238";
    protected static final String BANK_CODE = "4410010";
    protected static final String TERMINAL_ID = "TMC0000000000001";
    protected static final String SWITCHER_ID = "0000000";


    protected static final String SIGN_ON = "001";
    protected static final String SIGN_OFF = "002";
    protected static final String ECHO_TEST = "003";
    protected static String date14() {
        return new SimpleDateFormat("YYYYMMDDhhmm").format(new Date()) +""+new SimpleDateFormat("ss").format(new Date()).substring(0,1);
    }

    protected static String date8()
    {
        return new SimpleDateFormat("ddhhmmss").format(new Date());
    }

    protected static String Stan() {
        Random random = new Random(System.nanoTime());
        int randomInt = random.nextInt(1000000000);
        int random2 = random.nextInt(1000000000);
        String first = String.valueOf(randomInt) + String.valueOf(random2);
        return first.substring(0, 12);
    }
    protected static String padLeftWithZero(String value,int length){
        return StringUtils.leftPad(value,length,"0");
    }

    protected static String padRightWithZero(String value,int length){
        return StringUtils.rightPad(value,length,"0");
    }
    protected static String padLeftWithSpace(String value,int length){
        return StringUtils.leftPad(value,length," ");
    }

    protected static String padRightWithSpace(String value,int length){
        return StringUtils.rightPad(value,length," ");
    }

    protected static String numberToPay(double number){
        int a = (int) number;
        return String.valueOf(a);
    }

    protected static String amountToPay(double amount) {
        String curency = "360";
        String minor = "";
        String harga ="";
        if(amount % 1 == 0){
            minor = "0";
            harga = String.valueOf(Integer.toString((int)amount));
        }else{
            minor = "2";
            harga = String.valueOf(Integer.toString((int)(amount * 100)));
        }
        return curency + minor + StringUtils.leftPad(harga,12,"0");
    }

    protected static String valueToMinor(double amount,int minor) {
        if(amount % 1 == 0){
            StringBuilder s = new StringBuilder();
            for(int i=1;i<= minor;i++){
                s.append("0");
            }

            return new DecimalFormat("#").format(amount) + s.toString();
        }else{
            StringBuilder s = new StringBuilder();
            s.append("1");
            for(int i=1;i<= minor;i++) {
                s.append("0");
            }
            return new DecimalFormat("#").format(amount * Integer.parseInt(s.toString()));
        }
    }

    protected double numberMinorUnit(String val, int min) {
        int bagi = Integer.parseInt("1" + StringUtils.leftPad("", min, "0"));
        return Double.parseDouble(val) / bagi;
    }
}
