package main.java.com.trimindi.switching.utils.generator;

import main.java.com.trimindi.switching.packager.BukopinPackager;
import main.java.com.trimindi.switching.response.postpaid.InquiryResponse;
import main.java.com.trimindi.switching.services.transaction.models.Transaction;
import main.java.com.trimindi.switching.utils.iso.builder.FieldBuilder;
import main.java.com.trimindi.switching.utils.iso.builder.ISOMsgBuilder;
import main.java.com.trimindi.switching.utils.iso.models.Rules;
import main.java.com.trimindi.switching.utils.iso.parsing.SDE;
import main.java.com.trimindi.switching.utils.rules.response.ResponseRulesGeneratorPostPaid;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by HP on 15/05/2017.
 */
public class PostPaidGenerator extends BaseHelper {

    public static ISOMsg generateReversal(ISOMsg isoMsg, String MTI) throws ISOException {
        isoMsg.setPackager(new BukopinPackager());

        ISOMsg newMsg = (ISOMsg) isoMsg.clone();
        newMsg.setPackager(new BukopinPackager());
        newMsg.setMTI(MTI);
        newMsg.set(11, Stan());
        newMsg.set(12, date14());
        newMsg.set(56, new FieldBuilder.Builder()
                .addValue(isoMsg.getMTI(), 4, "0", "L")
                .addValue(isoMsg.getString(11), 12, "0", "L")
                .addValue(isoMsg.getString(12), 14, "0", "L")
                .addValue(isoMsg.getString(32), 7, "0", "L")
                .build());
        return newMsg;
    }

    public static ISOMsg generatePurchase(Transaction t) throws ISOException {
        String payload = t.getINQUIRY();
        ISOMsg inquiry = new ISOMsg();
        inquiry.setPackager(new BukopinPackager());
        inquiry.unpack(payload.getBytes());
        List<Rules> bit48 = new SDE.Builder()
                .setPayload(inquiry.getString(48))
                .setRules(ResponseRulesGeneratorPostPaid.postPaidInquiryResponse(48,true))
                .generate();
        InquiryResponse inquiryResponse = new InquiryResponse(bit48,true);
        return new ISOMsgBuilder.Builder("2200")
                .addPackager(new BukopinPackager())
                .addField(2, PAN_POSTPAID)
                .addField(4, "3600" + padLeftWithZero(valueToMinor(t.getTAGIHAN() + t.getADMIN() + t.getPINALTY(), 0), 12))
                .addField(11,Stan())
                .addField(12,date14())
                .addField(15,date8())
                .addField(26, t.getMERCHANT_ID())
                .addField(32,BANK_CODE)
                .addField(33,PARTNER_ID)
                .addField(41,TERMINAL_ID)
                .addField(48, new FieldBuilder.Builder()
                        .addValue(SWITCHER_ID,7,"","L")
                        .addValue(t.getMSSIDN(),12,"0","L")
                        .addValue(String.valueOf(inquiryResponse.getBillStatus()),1,"0","R")
                        .addValue(String.valueOf(inquiryResponse.getBillStatus()),1,"0","R")
                        .addValue(String.valueOf(inquiryResponse.getTotalOutstandingBill()),2,"0","L")
                        .addValue(inquiryResponse.getBukopinTbkReferenceNumber(),32," ","R")
                        .addValue(inquiryResponse.getSubscriberName(),25," ","R")
                        .addValue(inquiryResponse.getSubscriberUnit(), 5, "0", "L")
                        .addValue(inquiryResponse.getServiceUnitPhone(), 15, "0", "L")
                        .addValue(inquiryResponse.getSubscriberSegmentation(),4," ","R")
                        .addValue(String.valueOf(inquiryResponse.getPowerConsumingCategory()),9,"0","L")
                        .addValue(new DecimalFormat("#").format(inquiryResponse.getTotaladminChargers()),9,"0","L")
                        .addValue(String.valueOf(inquiryResponse.getBillPeriodrepeated()), 6, "0", "L")
                        .addValue(String.valueOf(inquiryResponse.getDueDaterepeated()),8,"0","L")
                        .addValue(String.valueOf(inquiryResponse.getMeterReadDaterepeated()),8,"0","L")
                        .addValue(new DecimalFormat("#").format(inquiryResponse.getTotalElectricityBillrepeated()),12,"0","L")
                        .addValue(String.valueOf(inquiryResponse.getIncentiverepeated()),11,"0","R")
                        .addValue(new DecimalFormat("#").format(inquiryResponse.getAddedTaxrepeated()),10,"0","L")
                        .addValue(new DecimalFormat("#").format(inquiryResponse.getPenaltyFeerepeated()),12,"0","L")
                        .addValue(String.valueOf(inquiryResponse.getPreviousMeterReading1()), 8, "0", "L")
                        .addValue(String.valueOf((int) inquiryResponse.getCurrentMeterReading1()), 8, "0", "L")
                        .addValue(String.valueOf(inquiryResponse.getPreviousMeterReading2repeated()), 8, "0", "L")
                        .addValue(String.valueOf(inquiryResponse.getCurrentMeterReading2repeated()), 8, "0", "L")
                        .addValue(String.valueOf(inquiryResponse.getPreviousMeterReading3repeated()), 8, "0", "L")
                        .addValue(String.valueOf(inquiryResponse.getCurrentMeterReading3repeated()), 8, "0", "L")
                        .build())
                .addField(61, t.getBUKOPINREF())
                .build();
    }



    public static ISOMsg generateInquiry(String MACHINE_TYPE, String MSSIDN) throws ISOException {
        return new ISOMsgBuilder.Builder("2100")
                .addPackager(new BukopinPackager())
                .addField(2,PAN_POSTPAID)
                .addField(11,Stan())
                .addField(12,date14())
                .addField(26,MACHINE_TYPE)
                .addField(32,BANK_CODE)
                .addField(33,PARTNER_ID)
                .addField(41,TERMINAL_ID)
                .addField(48, new FieldBuilder.Builder()
                        .addValue(SWITCHER_ID,7,"","L")
                        .addValue(MSSIDN,12,"0","R")
                        .addValue("1",1,"0","R")
                        .build())
                .build();
    }
}
