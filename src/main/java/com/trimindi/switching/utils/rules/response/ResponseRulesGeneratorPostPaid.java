package main.java.com.trimindi.switching.utils.rules.response;

import main.java.com.trimindi.switching.utils.iso.models.Rules;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HP on 14/05/2017.
 */
public class ResponseRulesGeneratorPostPaid {
    public static List<Rules> postPaidInquiryResponse(int bitnumber,boolean success) {
        List<Rules> R = new ArrayList<>();
        switch (bitnumber){
            case 48:
                R.clear();
                R.add(new Rules<String>(1, "Switcher ID",  7));
                R.add(new Rules<Integer>(2, "Subscriber ID",  12));
                R.add(new Rules<Integer>(3, "Bill Status (BS)" , 1));
                /**
                 * IF RC 0000 = SUCCESS
                 */
                if(success){
                    R.add(new Rules<Integer>(4, "Total Outstanding Bill", 2));
                    R.add(new Rules<String>(5, "PT.Bank Bukopin, Tbk Reference Number", 32));
                    R.add(new Rules<String>(6, "Subscriber Name", 25));
                    R.add(new Rules<String>(7, "Subscriber Unit (SU)", 5));
                    R.add(new Rules<String>(8, "Service Unit Phone (SUP)", 15));
                    R.add(new Rules<String>(9, "Subscriber Segmentation", 4));
                    R.add(new Rules<Integer>(10, "Power Consuming Category",  9));
                    R.add(new Rules<Integer>(11, "Total admin Chargers",  9));
                    R.add(new Rules<Integer>(12, "Bill Period (repeated) ",  6));
                    R.add(new Rules<Integer>(13, "Due Date (repeated)",  8));
                    R.add(new Rules<Integer>(14, "Meter Read Date (repeated)",  8));
                    R.add(new Rules<Integer>(15, "Total Electricity Bill (repeated)",  12));
                    R.add(new Rules<String>(16, "Incentive (repeated)", 11));
                    R.add(new Rules<Integer>(17, "Value Added Tax* (repeated)", 10));
                    R.add(new Rules<Integer>(18, "Penalty Fee (repeated)",  12));
                    R.add(new Rules<Integer>(19, "Previous Meter Reading 1 (repeated)",  8));
                    R.add(new Rules<Integer>(20, "Current Meter Reading 1 (repeated)", 8));
                    R.add(new Rules<Integer>(21, "Previous Meter Reading 2(repeated)",  8));
                    R.add(new Rules<Integer>(22, "Current Meter Reading 2 (repeated)",  8));
                    R.add(new Rules<Integer>(23, "Previous Meter Reading 3(repeated)",  8));
                    R.add(new Rules<Integer>(24, "Current Meter Reading 3 (repeated)",  8));
                }

                break;
        }
        return R;
    }

    public static List<Rules> postPaidPaymentResponse(int bitnumber,boolean success){
        List<Rules> R = new ArrayList<>();
        switch (bitnumber){
            case 48:
                R.clear();
                R.add(new Rules<String>(1, "Switcher ID",  7));
                R.add(new Rules<Integer>(2, "Subscriber ID",  12));
                R.add(new Rules<Integer>(3, "Bill Status (BS)", 1));
                R.add(new Rules<Integer>(4, "Payment Status (PS)", 1));
                /**
                 * IF RC 0000 = SUCCESS
                 */
                if(success){
                    R.add(new Rules<Integer>(5, "Total Outstanding Bill", 2));
                    R.add(new Rules<String>(6, "PT.Bank Bukopin, Tbk Reference Number", 32));
                    R.add(new Rules<String>(7, "Subscriber Name", 25));
                    R.add(new Rules<String>(8, "Subscriber Unit (SU)", 5));
                    R.add(new Rules<String>(9, "Service Unit Phone (SUP)", 15));
                    R.add(new Rules<String>(10, "Subscriber Segmentation", 4));
                    R.add(new Rules<Integer>(11, "Power Consuming Category", 9));
                    R.add(new Rules<Integer>(12, "Total admin Chargers",  9));
                    R.add(new Rules<Integer>(13, "Bill Period (repeated) ",  6));
                    R.add(new Rules<Integer>(14, "Due Date (repeated)",  8));
                    R.add(new Rules<Integer>(15, "Meter Read Date (repeated)",  8));
                    R.add(new Rules<Integer>(16, "Total Electricity Bill (repeated)",  12));
                    R.add(new Rules<String>(17, "Incentive (repeated)",  11));
                    R.add(new Rules<Integer>(18, "Value Added Tax* (repeated)", 10));
                    R.add(new Rules<Integer>(19, "Penalty Fee (repeated)",  12));
                    R.add(new Rules<Integer>(20, "Previous Meter Reading 1 (repeated)",  8));
                    R.add(new Rules<Integer>(21, "Current Meter Reading 1 (repeated)", 8));
                    R.add(new Rules<Integer>(22, "Previous Meter Reading 2(repeated)",  8));
                    R.add(new Rules<Integer>(23, "Current Meter Reading 2 (repeated)",  8));
                    R.add(new Rules<Integer>(24, "Previous Meter Reading 3(repeated)",  8));
                    R.add(new Rules<Integer>(25, "Current Meter Reading 3 (repeated)",  8));
                }
                break;
        }
        return R;
    }

    public static List<Rules> postPaidReversalResponse(int bitnumber,boolean success){
        List<Rules> R = new ArrayList<>();
        switch (bitnumber){
            case 48:
                R.clear();
                R.add(new Rules<String>(1, "Switcher ID",  7));
                R.add(new Rules<Integer>(2, "Subscriber ID",  12));
                R.add(new Rules<Integer>(3, "Bill Status (BS)", 1));
                R.add(new Rules<Integer>(4, "Payment Status (PS)", 1));
                /**
                 * IF RC 0000 = SUCCESS
                 */
                if(success){
                    R.add(new Rules<Integer>(5, "Total Outstanding Bill", 2));
                    R.add(new Rules<String>(6, "PT.Bank Bukopin, Tbk Reference Number", 32));
                    R.add(new Rules<String>(7, "Subscriber Name", 25));
                    R.add(new Rules<String>(8, "Subscriber Unit (SU)", 5));
                    R.add(new Rules<String>(9, "Service Unit Phone (SUP)", 15));
                    R.add(new Rules<String>(10, "Subscriber Segmentation",  4));
                    R.add(new Rules<Integer>(11, "Power Consuming Category",  9));
                    R.add(new Rules<Integer>(12, "Total admin Chargers",  9));
                    R.add(new Rules<Integer>(13, "Bill Period (repeated) ", 6));
                    R.add(new Rules<Integer>(14, "Due Date (repeated)",  8));
                    R.add(new Rules<Integer>(15, "Meter Read Date (repeated)",  8));
                    R.add(new Rules<Integer>(16, "Total Electricity Bill (repeated)",  12));
                    R.add(new Rules<String>(17, "Incentive (repeated)",  11));
                    R.add(new Rules<Integer>(18, "IsoField Added Tax* (repeated)",  10));
                    R.add(new Rules<Integer>(19, "Penalty Fee (repeated)",  12));
                    R.add(new Rules<Integer>(20, "Previous Meter Reading 1 (repeated)",  8));
                    R.add(new Rules<Integer>(21, "Administration Charge (AC)",  9));
                    R.add(new Rules<Integer>(22, "Previous Meter Reading 2(repeated)",  8));
                    R.add(new Rules<Integer>(23, "Current Meter Reading 2 (repeated)",  8));
                    R.add(new Rules<Integer>(24, "Previous Meter Reading 3(repeated)",  8));
                    R.add(new Rules<Integer>(25, "Current Meter Reading 3 (repeated)",  8));
                }
                break;
            case 56:
                R.clear();
                R.add(new Rules<Integer>(1, "Original MTI",  4));
                R.add(new Rules<Integer>(2, "Original System Trace Audit Number (STAN)",  12));
                R.add(new Rules<Integer>(3, "Original Date & Time Local Transaction", 14));
                R.add(new Rules<Integer>(4, "Original Bank Code", 7));
                break;
        }
        return R;
    }
}
