package main.java.com.trimindi.switching.utils.constanta;

/**
 * Created by HP on 20/05/2017.
 */
public class TStatus {
    public static final String INQUIRY = "00";
    public static final String PAYMENT_PROSESS = "01";
    public static final String PAYMENT_SUCCESS = "02";
    public static final String REVERSE_PAYMENT_SUCCESS = "11";
    public static final String SUCCESS_BUT_NOT_RESPONDED = "04";
}
