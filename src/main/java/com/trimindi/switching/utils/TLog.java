package main.java.com.trimindi.switching.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by HP on 18/05/2017.
 */
public class TLog {
    private File logFile;
    private String date;
    public TLog() {
    }
    public TLog(String fileName) {
        date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        logFile = new File("recon/"+date+"_"+fileName);
    }

    public TLog(File f) {
        logFile = f;
    }

    public File getLogFile() {
        return logFile;
    }

    public void setLogFile(String data) {
        logFile = new File(data);
    }

    public void log(String s) {
        try {
            FileWriter fw = new FileWriter(this.logFile,true);
            String date = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss").format(new Date());
            fw.write(date+" : "+s);
            fw.write(System.lineSeparator());
            fw.close();
        } catch (IOException ex) {
            System.err.println("Couldn't log this: "+s);
        }
    }
}
