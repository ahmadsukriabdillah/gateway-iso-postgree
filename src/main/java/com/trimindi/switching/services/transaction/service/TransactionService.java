package main.java.com.trimindi.switching.services.transaction.service;

import main.java.com.trimindi.switching.services.hibernate.SessionUtils;
import main.java.com.trimindi.switching.services.transaction.models.Transaction;
import main.java.com.trimindi.switching.utils.constanta.TStatus;
import org.hibernate.Query;
import org.hibernate.Session;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by sx on 14/05/17.
 */
public class TransactionService {

    public TransactionService() {

    }


    public void persist(Transaction entity) {
        Session session = SessionUtils.getSession();
        org.hibernate.Transaction tx = session.beginTransaction();
        session.persist(entity);
        tx.commit();
        session.close();
    }

    public void update(Transaction entity) {
        Session session = SessionUtils.getSession();
        org.hibernate.Transaction tx = session.beginTransaction();
        session.update(entity);
        tx.commit();
        session.close();
    }

    public Transaction findById(String id) {
        Session session = SessionUtils.getSession();
        Transaction tx = (Transaction) session.get(Transaction.class,id);
        session.close();
        return tx;
    }

    public Transaction findByMSSIDN(String mssidn){
        Session session = SessionUtils.getSession();
        Timestamp time = new Timestamp(System.currentTimeMillis());
        Timestamp timeEnd = new Timestamp(time.getTime());
        timeEnd.setTime(time.getTime() + 2 * 60 * 1000);
        Query q = session.getNamedQuery("transaction.findByMssidn")
                .setParameter("mssidn",mssidn)
                .setParameter("dateStart",time)
                .setParameter("dateEnd",timeEnd);
        Transaction transaction = (Transaction) q.uniqueResult();
        session.close();
        return transaction;
    }


    public void updateToUnderProsess(Transaction transaction) {
        Session session = SessionUtils.getSession();
        org.hibernate.Transaction tx = session.beginTransaction();
        transaction.setST(TStatus.PAYMENT_PROSESS);
        session.update(transaction);
        tx.commit();
        session.close();
    }

    public List<Transaction> findByBulanDanTahun(String bulan, String tahun,String partnerid) {
        Session session = SessionUtils.getSession();
        List<Transaction> transaction = session.getNamedQuery("transaction.findByBulanDanTahun")
                .setParameter("BULAN",bulan)
                .setParameter("TAHUN",tahun)
                .setParameter("USER_ID",partnerid)
                .list();
        session.close();
            return transaction;
    }

}
