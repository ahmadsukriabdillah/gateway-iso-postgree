package main.java.com.trimindi.switching.services.transaction.models;

import javax.persistence.*;

/**
 * Created by HP on 20/05/2017.
 */
@Entity
@Table(name = "mst_status")
public class TransactionStatus {
    @Id
    private String status_id;
    @Column(name = "keterangan")
    private String status_transaksi;

    public String getStatus_id() {
        return status_id;
    }

    public TransactionStatus setStatus_id(String status_id) {
        this.status_id = status_id;
        return this;
    }

    public String getStatus_transaksi() {
        return status_transaksi;
    }

    public TransactionStatus setStatus_transaksi(String status_transaksi) {
        this.status_transaksi = status_transaksi;
        return this;
    }

}
