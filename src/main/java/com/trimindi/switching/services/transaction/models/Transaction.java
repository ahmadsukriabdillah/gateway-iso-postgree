package main.java.com.trimindi.switching.services.transaction.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.NamedNativeQueries;
import org.hibernate.annotations.NamedNativeQuery;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

/**
 * Created by sx on 14/05/17.
 */
@Entity
@Table(name = "mst_transaksi")
@NamedNativeQueries({
        @NamedNativeQuery(
                name = "transaction.findByMssidn",
                query = "select * from mst_transaksi t where t.MSSIDN = :mssidn and t.DATE > :dateStart and t.date < :dateEnd",
                resultClass = Transaction.class
        ),
        @NamedNativeQuery(
                name = "transaction.findByBulanDanTahun",
                query = "select * from mst_transaksi t where t.BULAN = :BULAN and t.TAHUN = :TAHUN and t.USER_ID = :USER_ID ORDER BY t.DATE ASC",
                resultClass = Transaction.class
        )
})
@XmlRootElement
public class Transaction {


    @Id
    private String NTRANS;
    private String MSSIDN;
    private String BULAN;
    private String TAHUN;
    private String NAMA_PELANGGAN;
    private String MERCHANT_ID;
    @XmlElement(nillable=true)
    private Timestamp DATE = null;
    @XmlElement(nillable=true)
    private Timestamp INQDATE = null;
    @XmlElement(nillable=true)
    private Timestamp PAYDATE = null;
    @XmlElement(nillable=true)
    private Timestamp REVERSEDATE = null;
    private String PRODUCT_CODE;
    private String DENOM;
    private String PLNREF;
    private String BUKOPINREF;
    private double TAGIHAN;
    private double ADMIN;
    private double PINALTY;
    private double TOTAL;
    private double FEE;
    private double DEBET;
    private double KREDIT;
    private double SALDO;
    private String INQUIRY;
    private String PAYMENT;
    private String REVERSE;
    private String USER_ID;
    private String PARTNER_ID;
    private String MAC;
    private String IPADDRESS;
    private String LATITUDE;
    private String LONGITUDE;
    private String TOKEN;
    private String ST;
    private int PRT;

    public Transaction() {
        this.BULAN = new SimpleDateFormat("MM").format(new java.util.Date());
        this.TAHUN = new SimpleDateFormat("YYYY").format(new java.util.Date());
        this.DATE = new Timestamp(System.currentTimeMillis());
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "NTRANS='" + NTRANS + '\'' +
                ", MSSIDN='" + MSSIDN + '\'' +
                ", BULAN='" + BULAN + '\'' +
                ", TAHUN='" + TAHUN + '\'' +
                ", NAMA_PELANGGAN='" + NAMA_PELANGGAN + '\'' +
                ", MERCHANT_ID='" + MERCHANT_ID + '\'' +
                ", DATE=" + DATE +
                ", INQDATE=" + INQDATE +
                ", PAYDATE=" + PAYDATE +
                ", REVERSEDATE=" + REVERSEDATE +
                ", PRODUCT_CODE='" + PRODUCT_CODE + '\'' +
                ", DENOM='" + DENOM + '\'' +
                ", PLNREF='" + PLNREF + '\'' +
                ", BUKOPINREF='" + BUKOPINREF + '\'' +
                ", TAGIHAN=" + TAGIHAN +
                ", ADMIN=" + ADMIN +
                ", PINALTY=" + PINALTY +
                ", TOTAL=" + TOTAL +
                ", FEE=" + FEE +
                ", DEBET=" + DEBET +
                ", KREDIT=" + KREDIT +
                ", SALDO=" + SALDO +
                ", INQUIRY='" + INQUIRY + '\'' +
                ", PAYMENT='" + PAYMENT + '\'' +
                ", REVERSE='" + REVERSE + '\'' +
                ", USER_ID='" + USER_ID + '\'' +
                ", PARTNER_ID='" + PARTNER_ID + '\'' +
                ", MAC='" + MAC + '\'' +
                ", IPADDRESS='" + IPADDRESS + '\'' +
                ", LATITUDE='" + LATITUDE + '\'' +
                ", LONGITUDE='" + LONGITUDE + '\'' +
                ", TOKEN='" + TOKEN + '\'' +
                ", ST='" + ST + '\'' +
                ", PRT=" + PRT +
                '}';
    }

    public Timestamp getINQDATE() {
        return INQDATE;
    }

    public Transaction setINQDATE(Timestamp INQDATE) {
        this.INQDATE = INQDATE;
        return this;
    }

    public Timestamp getPAYDATE() {
        return PAYDATE;
    }

    public Transaction setPAYDATE(Timestamp PAYDATE) {
        this.PAYDATE = PAYDATE;
        return this;
    }

    public Timestamp getREVERSEDATE() {
        return REVERSEDATE;
    }

    public Transaction setREVERSEDATE(Timestamp REVERSEDATE) {
        this.REVERSEDATE = REVERSEDATE;
        return this;
    }

    @XmlTransient @JsonIgnore
    public String getPAYMENT() {
        return PAYMENT;
    }

    public Transaction setPAYMENT(String PAYMENT) {
        this.PAYMENT = PAYMENT;
        return this;
    }

    @XmlTransient @JsonIgnore
    public String getREVERSE() {
        return REVERSE;
    }

    public Transaction setREVERSE(String REVERSE) {
        this.REVERSE = REVERSE;
        return this;
    }

    public int getPRT() {
        return PRT;
    }

    public Transaction setPRT(int PRT) {
        this.PRT = PRT;
        return this;
    }

    public String getBULAN() {
        return BULAN;
    }

    public Transaction setBULAN(String BULAN) {
        this.BULAN = BULAN;
        return this;
    }

    public String getTAHUN() {
        return TAHUN;
    }

    public Transaction setTAHUN(String TAHUN) {
        this.TAHUN = TAHUN;
        return this;
    }

    public String getPARTNER_ID() {
        return PARTNER_ID;
    }

    public Transaction setPARTNER_ID(String PARTNER_ID) {
        this.PARTNER_ID = PARTNER_ID;
        return this;
    }

    public String getMAC() {
        return MAC;
    }

    public Transaction setMAC(String MAC) {
        this.MAC = MAC;
        return this;
    }

    public String getIPADDRESS() {
        return IPADDRESS;
    }

    public Transaction setIPADDRESS(String IPADDRESS) {
        this.IPADDRESS = IPADDRESS;
        return this;
    }

    public String getLATITUDE() {
        return LATITUDE;
    }

    public Transaction setLATITUDE(String LATITUDE) {
        this.LATITUDE = LATITUDE;
        return this;
    }

    public String getLONGITUDE() {
        return LONGITUDE;
    }

    public Transaction setLONGITUDE(String LONGITUDE) {
        this.LONGITUDE = LONGITUDE;
        return this;
    }

    public String getNTRANS() {
        return NTRANS;
    }
    public Transaction setNTRANS(String NTRANS) {
        this.NTRANS = NTRANS;
        return this;
    }

    public String getNAMA_PELANGGAN() {
        return NAMA_PELANGGAN;
    }

    public Transaction setNAMA_PELANGGAN(String NAMA_PELANGGAN) {
        this.NAMA_PELANGGAN = NAMA_PELANGGAN;
        return this;
    }

    public String getMSSIDN() {
        return MSSIDN;
    }

    public Transaction setMSSIDN(String MSSIDN) {
        this.MSSIDN = MSSIDN;
        return this;
    }

    public String getMERCHANT_ID() {
        return MERCHANT_ID;
    }

    public Transaction setMERCHANT_ID(String MERCHANT_ID) {
        this.MERCHANT_ID = MERCHANT_ID;
        return this;
    }

    public Timestamp getDATE() {
        return DATE;
    }

    public Transaction setDATE(Timestamp DATE) {
        this.DATE = DATE;
        return this;
    }

    public String getPRODUCT_CODE() {
        return PRODUCT_CODE;
    }

    public Transaction setPRODUCT_CODE(String PRODUCT_CODE) {
        this.PRODUCT_CODE = PRODUCT_CODE;
        return this;
    }

    public String getDENOM() {
        return DENOM;
    }

    public Transaction setDENOM(String DENOM) {
        this.DENOM = DENOM;
        return this;
    }

    public String getPLNREF() {
        return PLNREF;
    }

    public Transaction setPLNREF(String PLNREF) {
        this.PLNREF = PLNREF;
        return this;
    }

    public String getBUKOPINREF() {
        return BUKOPINREF;
    }

    public Transaction setBUKOPINREF(String BUKOPINREF) {
        this.BUKOPINREF = BUKOPINREF;
        return this;
    }

    public double getTAGIHAN() {
        return TAGIHAN;
    }

    public Transaction setTAGIHAN(double TAGIHAN) {
        this.TAGIHAN = TAGIHAN;
        return this;
    }

    public double getADMIN() {
        return ADMIN;
    }

    public Transaction setADMIN(double ADMIN) {
        this.ADMIN = ADMIN;
        return this;
    }

    public double getPINALTY() {
        return PINALTY;
    }

    public Transaction setPINALTY(double PINALTY) {
        this.PINALTY = PINALTY;
        return this;
    }

    public double getTOTAL() {
        return TOTAL;
    }

    public Transaction setTOTAL(double TOTAL) {
        this.TOTAL = TOTAL;
        return this;
    }

    public double getFEE() {
        return FEE;
    }

    public Transaction setFEE(double FEE) {
        this.FEE = FEE;
        return this;
    }

    public double getDEBET() {
        return DEBET;
    }

    public Transaction setDEBET(double DEBET) {
        this.DEBET = DEBET;
        return this;
    }

    public double getKREDIT() {
        return KREDIT;
    }

    public Transaction setKREDIT(double KREDIT) {
        this.KREDIT = KREDIT;
        return this;
    }

    public double getSALDO() {
        return SALDO;
    }

    public Transaction setSALDO(double SALDO) {
        this.SALDO = SALDO;
        return this;
    }

    @XmlTransient @JsonIgnore
    public String getINQUIRY() {
        return INQUIRY;
    }

    public Transaction setINQUIRY(String RAW) {
        this.INQUIRY = RAW;
        return this;
    }

    public String getUSER_ID() {
        return USER_ID;
    }

    public Transaction setUSER_ID(String USER_ID) {
        this.USER_ID = USER_ID;
        return this;
    }

    public String getTOKEN() {
        return TOKEN;
    }

    public Transaction setTOKEN(String TOKEN) {
        this.TOKEN = TOKEN;
        return this;
    }

    public String getST() {
        return ST;
    }

    public Transaction setST(String ST) {
        this.ST = ST;
        return this;
    }
}
