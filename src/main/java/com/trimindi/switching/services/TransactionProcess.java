package main.java.com.trimindi.switching.services;

import main.java.com.trimindi.switching.packager.BukopinPackager;
import main.java.com.trimindi.switching.response.prepaid.InquiryResponse;
import main.java.com.trimindi.switching.services.partner.models.PartnerCredential;
import main.java.com.trimindi.switching.services.partner.models.PartnerDeposit;
import main.java.com.trimindi.switching.services.partner.models.PartnerFee;
import main.java.com.trimindi.switching.services.partner.services.PartnerDepositService;
import main.java.com.trimindi.switching.services.partner.services.PartnerFeeService;
import main.java.com.trimindi.switching.services.product_code.models.ProductCode;
import main.java.com.trimindi.switching.services.transaction.models.Transaction;
import main.java.com.trimindi.switching.services.transaction.service.TransactionService;
import main.java.com.trimindi.switching.utils.constanta.Constanta;
import main.java.com.trimindi.switching.utils.constanta.TStatus;
import main.java.com.trimindi.switching.utils.iso.models.Rules;
import main.java.com.trimindi.switching.utils.iso.parsing.SDE;
import main.java.com.trimindi.switching.utils.rules.response.ResponseRulesGeneratorPrePaid;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by HP on 19/05/2017.
 */
public class TransactionProcess {
    private TransactionService transactionService;
    private PartnerFeeService partnerFeeService;
    private PartnerDepositService partnerDepositService;

    public TransactionProcess(){
        transactionService= new TransactionService();
        partnerFeeService = new PartnerFeeService();
        partnerDepositService = new PartnerDepositService();
    }

    public InquiryResponse inquiryPrepaid(ISOMsg inquiry,PartnerCredential partnerCredential, Map<String,String> map,ProductCode productCode) throws ISOException {
        inquiry.setPackager(new BukopinPackager());
        Transaction transaction= new Transaction();

        List<Rules> bit48 = new SDE.Builder()
                .setPayload(inquiry.getString(48))
                .setRules(ResponseRulesGeneratorPrePaid.prePaidInquiryResponse(48,true))
                .generate();

        List<Rules> bit62 = new SDE.Builder()
                .setPayload(inquiry.getString(62))
                .setRules(ResponseRulesGeneratorPrePaid.prePaidInquiryResponse(62,true))
                .generate();
        bit48.addAll(bit62);
        InquiryResponse response = new InquiryResponse(bit48,true);
        /**
         * MT = MACHINE_TYPE
         * PC = PRODUDUCT CODE
         * DENOM = DENOM
         */
        Random random = new Random(System.currentTimeMillis());
        random.nextInt(999999999);
        PartnerFee partnerFee = partnerFeeService.findPartnerID(partnerCredential.getPartner_id());
        PartnerDeposit partnerDeposit = partnerDepositService.findPartnerDeposit(partnerFee.getPartner_id());
        String ntrans = new SimpleDateFormat("YYYYMMDDhhmmss").format(new java.util.Date()) + random.nextInt(999999999);
        transaction
                .setNTRANS(ntrans)
                .setADMIN(response.getAdminCharge())
                .setNAMA_PELANGGAN(response.getSubscriberName())
                .setBUKOPINREF(response.getBukopinReferenceNumber())
                .setINQDATE(new Timestamp(System.currentTimeMillis()))
                .setPLNREF(response.getPLNReferenceNumber())
                .setMSSIDN(response.getSubscriberID())
                .setMERCHANT_ID(map.get(Constanta.MACHINE_TYPE))
                .setPRODUCT_CODE(productCode.getProduct_item())
                .setTAGIHAN(productCode.getNominal())
                .setFEE(partnerFee.getFee())
                .setUSER_ID(partnerCredential.getPartner_uid())
                .setDENOM(map.get(Constanta.DENOM))
                .setINQUIRY(new String(inquiry.pack()))
                .setTOTAL(response.getAdminCharge() + productCode.getNominal())
                .setDEBET(response.getAdminCharge() + productCode.getNominal())
                .setMAC(map.get(Constanta.MAC))
                .setIPADDRESS(map.get(Constanta.IP_ADDRESS))
                .setPARTNER_ID(partnerCredential.getPartner_id())
                .setLATITUDE(map.get(Constanta.LATITUDE))
                .setLONGITUDE(map.get(Constanta.LONGITUDE))
                .setPINALTY(0)
                .setST(TStatus.INQUIRY);
        response.setFee(partnerFee.getFee()).setNtrans(ntrans);
        response.setSaldo(partnerDeposit.getSaldo());
        response.setHarga(productCode.getNominal());
        transactionService.persist(transaction);
        return response;
    }

}
