package main.java.com.trimindi.switching.services.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.jpos.q2.QBeanSupport;
import org.jpos.util.NameRegistrar;

import java.io.File;

/**
 * Created by HP on 30/05/2017.
 */
public class SessionUtils extends QBeanSupport {
    private static SessionUtils instance=new SessionUtils();
    private static SessionFactory sessionFactory;
    public static SessionUtils getInstance(){
        return instance;
    }
    private static ServiceRegistry serviceRegistry;

    public SessionUtils() {

    }

    public static synchronized  SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            File hibernatePropsFile = new File("src/main/resources/hibernate.cfg.xml");
            Configuration configuration = new Configuration();
            configuration.configure(hibernatePropsFile);
            serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        }
        return sessionFactory;
    }
    @Override
    protected void initService() throws Exception {
        try {
            String hibernatePropsFilePath = cfg.get("hibernate_cfg_path");
            File hibernatePropsFile = new File(hibernatePropsFilePath);
            Configuration configuration = new Configuration();
            configuration.configure(hibernatePropsFile);
//            ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(
//                    configuration.getProperties()). buildServiceRegistry();
            sessionFactory = configuration.buildSessionFactory();
            NameRegistrar.register("hibernate-manager", this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Session getSession(){
        Session session =  getInstance().getSessionFactory().openSession();
        return session;
    }
}
