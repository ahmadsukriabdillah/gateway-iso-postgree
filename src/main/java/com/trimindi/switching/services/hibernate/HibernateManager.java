package main.java.com.trimindi.switching.services.hibernate;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.jpos.q2.QBeanSupport;
import org.jpos.util.NameRegistrar;

import java.io.File;

/**
 * Created by sx on 13/04/17.
 */
public class HibernateManager extends QBeanSupport {

    private SessionFactory sessionFactory;

    @SuppressWarnings("deprecation")
    @Override
    protected void initService() throws Exception {
        try {
            String hibernatePropsFilePath = cfg.get("hibernate_cfg_path");
            File hibernatePropsFile = new File(hibernatePropsFilePath);

            Configuration configuration = new Configuration();
            configuration.configure(hibernatePropsFile);
            sessionFactory = configuration.buildSessionFactory();
            NameRegistrar.register("hibernate-manager", this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void shutdown() {
        if (getSessionFactory() != null) {
            getSessionFactory().close();
        }
    }
}