package main.java.com.trimindi.switching.services.merchant;

import main.java.com.trimindi.switching.services.hibernate.SessionUtils;
import org.hibernate.Session;

/**
 * Created by HP on 20/05/2017.
 */
public class MerchantService {
    public MerchantService(){

    }
    public boolean isAvailabel(String merchantid){
        Session session = SessionUtils.getSession();
        Merchant merchant = (Merchant) session.get(Merchant.class,merchantid);
        session.close();
        return (merchant != null);
    }
}
