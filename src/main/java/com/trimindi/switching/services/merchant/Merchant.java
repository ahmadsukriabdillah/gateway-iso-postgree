package main.java.com.trimindi.switching.services.merchant;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by HP on 20/05/2017.
 */
@Entity
@Table(name = "mst_merchant")
public class Merchant {

    @Id
    private String merchant_id;
    private String merchant_name;

    public String getMerchant_id() {
        return merchant_id;
    }

    public Merchant setMerchant_id(String merchant_id) {
        this.merchant_id = merchant_id;
        return this;
    }

    public String getMerchant_name() {
        return merchant_name;
    }

    public Merchant setMerchant_name(String merchant_name) {
        this.merchant_name = merchant_name;
        return this;
    }
}
