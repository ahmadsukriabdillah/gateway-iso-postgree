package main.java.com.trimindi.switching.services.partner.services;

import main.java.com.trimindi.switching.services.hibernate.SessionUtils;
import main.java.com.trimindi.switching.services.partner.models.TransaksiDeposit;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by HP on 22/05/2017.
 */
public class TransaksiDepositService {
    public TransaksiDepositService() {
    }

    public List<TransaksiDeposit> findAll(String partnerid){
        Session session = SessionUtils.getSession();
        Query transaksiDeposits = session.getNamedQuery("findTransaksiDepositByPartnerID").setString("partner_uid",partnerid);
        List<TransaksiDeposit> res = transaksiDeposits.list();
        session.close();
        return res;
    }
}
