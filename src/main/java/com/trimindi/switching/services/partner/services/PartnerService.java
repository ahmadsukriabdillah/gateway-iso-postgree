package main.java.com.trimindi.switching.services.partner.services;

import main.java.com.trimindi.switching.services.hibernate.SessionUtils;
import main.java.com.trimindi.switching.services.partner.models.Partner;
import org.hibernate.Session;

/**
 * Created by HP on 14/05/2017.
 */
public class PartnerService {

    public PartnerService(){
    }

    public Partner findById(String partnerid){
        Session session = SessionUtils.getSession();
        Partner p = (Partner) session.get(Partner.class,partnerid);
        session.close();
        return p;
    }
}
