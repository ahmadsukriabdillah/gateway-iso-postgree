package main.java.com.trimindi.switching.services.partner.services;

import main.java.com.trimindi.switching.services.hibernate.SessionUtils;
import main.java.com.trimindi.switching.services.partner.models.PartnerCredential;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

/**
 * Created by HP on 14/05/2017.
 */
public class PartnerCredentialService {
    public PartnerCredentialService() {

    }

    public PartnerCredential findByUserId(String id) {
        Session session = SessionUtils.getSession();
        PartnerCredential partnerCredential = (PartnerCredential) session.getNamedQuery("findPartnerCredentialByUID")
                .setParameter("partner_uid",id).uniqueResult();
        session.close();
        return partnerCredential;
    }

    public void update(PartnerCredential partnerCredential) {
        Session session = SessionUtils.getSession();
        Transaction tx = session.beginTransaction();
        session.update(partnerCredential);
        tx.commit();
        session.close();
    }
    public List<PartnerCredential> findAll() {
        Session session = SessionUtils.getSession();
        List<PartnerCredential> partnerCredential = session.getNamedQuery("findPartnerCredentialAll").list();
        session.close();
        return partnerCredential;
    }
}
