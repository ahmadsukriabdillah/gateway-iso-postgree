package main.java.com.trimindi.switching.services.partner.models;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by HP on 14/05/2017.
 */
@Entity
//@XmlRootElement
@Table(name = "mst_partner")
public class Partner {
    @Id
    @Column(name = "partner_id")
    private String partner_id;
    @Column(name = "partner_name")
    private String partner_name;
    @Column(name = "Phone")
    private String partner_phone;
    @Column(name = "address")
    private String partner_address;
    @Column(name = "e_mail")
    private String partner_email;
    @Column(name = "pic_id")
    private String partner_pic;


    public Partner() {
    }

    public Partner(String partner_id, String partner_name, String partner_phone, String partner_address, String partner_email, String partner_pic) {
        this.partner_id = partner_id;
        this.partner_name = partner_name;
        this.partner_phone = partner_phone;
        this.partner_address = partner_address;
        this.partner_email = partner_email;
        this.partner_pic = partner_pic;
    }

    public String getPartner_id() {
        return partner_id;
    }

    public void setPartner_id(String partner_id) {
        this.partner_id = partner_id;
    }

    public String getPartner_name() {
        return partner_name;
    }

    public void setPartner_name(String partner_name) {
        this.partner_name = partner_name;
    }

    public String getPartner_phone() {
        return partner_phone;
    }

    public void setPartner_phone(String partner_phone) {
        this.partner_phone = partner_phone;
    }

    public String getPartner_address() {
        return partner_address;
    }

    public void setPartner_address(String partner_address) {
        this.partner_address = partner_address;
    }

    public String getPartner_email() {
        return partner_email;
    }

    public void setPartner_email(String partner_email) {
        this.partner_email = partner_email;
    }

    public String getPartner_pic() {
        return partner_pic;
    }

    public void setPartner_pic(String partner_pic) {
        this.partner_pic = partner_pic;
    }
}
