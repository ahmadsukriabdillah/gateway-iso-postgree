package main.java.com.trimindi.switching.services.partner.services;

import main.java.com.trimindi.switching.services.hibernate.SessionUtils;
import main.java.com.trimindi.switching.services.partner.models.PartnerFee;
import org.hibernate.Session;

/**
 * Created by HP on 19/05/2017.
 */
public class PartnerFeeService {


    public PartnerFeeService(){
    }


    public PartnerFee findPartnerID(String partnerid){
        Session session = SessionUtils.getSession();
        PartnerFee partnerFee = (PartnerFee) session.get(PartnerFee.class,partnerid);
        session.close();
        return partnerFee;
    }
}
