package main.java.com.trimindi.switching.services.partner.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by HP on 14/05/2017.
 */
@Entity
@Table(name = "mst_partner_pic")
@XmlRootElement
public class PartnerPIC {
    @Id
    @Column(name = "pic_id")
    private String pic_id;
    @Column(name = "pic_name")
    private String pic_name;
    @Column(name = "address")
    private String pic_address;

    @Column(name = "Phone")
    private String pic_phone;
    @Column(name = "e_mail")
    private String pic_email;

    public PartnerPIC() {
    }

    public PartnerPIC(String pic_id, String pic_name, String pic_address, String pic_phone, String pic_email) {
        this.pic_id = pic_id;
        this.pic_name = pic_name;
        this.pic_address = pic_address;
        this.pic_phone = pic_phone;
        this.pic_email = pic_email;
    }

    public String getPic_id() {
        return pic_id;
    }

    public void setPic_id(String pic_id) {
        this.pic_id = pic_id;
    }

    public String getPic_name() {
        return pic_name;
    }

    public void setPic_name(String pic_name) {
        this.pic_name = pic_name;
    }

    public String getPic_address() {
        return pic_address;
    }

    public void setPic_address(String pic_address) {
        this.pic_address = pic_address;
    }

    public String getPic_phone() {
        return pic_phone;
    }

    public void setPic_phone(String pic_phone) {
        this.pic_phone = pic_phone;
    }

    public String getPic_email() {
        return pic_email;
    }

    public void setPic_email(String pic_email) {
        this.pic_email = pic_email;
    }
}
