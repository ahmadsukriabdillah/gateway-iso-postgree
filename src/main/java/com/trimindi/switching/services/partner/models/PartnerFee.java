package main.java.com.trimindi.switching.services.partner.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by HP on 19/05/2017.
 */
@Entity
@Table(name = "mst_fee")
public class PartnerFee {
    @Id
    private String partner_id;
    private double admin;
    private double fee;

    public PartnerFee() {
    }

    public String getPartner_id() {
        return partner_id;
    }

    public PartnerFee setPartner_id(String partner_id) {
        this.partner_id = partner_id;
        return this;
    }

    public double getAdmin() {
        return admin;
    }

    public PartnerFee setAdmin(double admin) {
        this.admin = admin;
        return this;
    }

    public double getFee() {
        return fee;
    }

    public PartnerFee setFee(double fee) {
        this.fee = fee;
        return this;
    }
}
