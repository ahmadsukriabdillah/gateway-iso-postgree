package main.java.com.trimindi.switching.services.partner.models;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by HP on 22/05/2017.
 */
@Entity
@Table(name = "trans_deposit")
@NamedNativeQueries({
        @NamedNativeQuery(
                name = "findTransaksiDepositByPartnerID",
                query = "select * from trans_deposit p where p.partner_id = :partner_uid",
                resultClass = TransaksiDeposit.class
        )
})
public class TransaksiDeposit {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "mst_trans_deposit")
    @SequenceGenerator(name = "mst_trans_deposit",sequenceName = "trans_deposit_squance",allocationSize = 1)
    @Column(name = "trans_id")
    private Integer id;
    private String partner_id;
    private String description;
    private double debet;
    private double kredit;
    private double saldo;
    private Timestamp date;

    public TransaksiDeposit() {
        date = new Timestamp(System.currentTimeMillis());
    }

    public Integer getId() {
        return id;
    }

    public TransaksiDeposit setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getPartner_id() {
        return partner_id;
    }

    public TransaksiDeposit setPartner_id(String partner_id) {
        this.partner_id = partner_id;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public TransaksiDeposit setDescription(String description) {
        this.description = description;
        return this;
    }

    public double getDebet() {
        return debet;
    }

    public TransaksiDeposit setDebet(double debet) {
        this.debet = debet;
        return this;
    }

    public double getKredit() {
        return kredit;
    }

    public TransaksiDeposit setKredit(double kredit) {
        this.kredit = kredit;
        return this;
    }

    public double getSaldo() {
        return saldo;
    }

    public TransaksiDeposit setSaldo(double saldo) {
        this.saldo = saldo;
        return this;
    }
}
