package main.java.com.trimindi.switching.services.partner.services;

import main.java.com.trimindi.switching.services.hibernate.SessionUtils;
import main.java.com.trimindi.switching.services.partner.models.PartnerDeposit;
import main.java.com.trimindi.switching.services.partner.models.TransaksiDeposit;
import main.java.com.trimindi.switching.services.transaction.models.Transaction;
import main.java.com.trimindi.switching.utils.constanta.TStatus;
import org.hibernate.Session;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;

import java.sql.Timestamp;

/**
 * Created by HP on 19/05/2017.
 */
public class PartnerDepositService {
    public PartnerDepositService(){

    }


    public boolean bookingSaldo(Transaction t){
        Session session = SessionUtils.getSession();
        session.getTransaction().begin();
        PartnerDeposit p = (PartnerDeposit) session.get(PartnerDeposit.class,t.getPARTNER_ID());
        if(p.getSaldo() < t.getTOTAL()){
            return false;
        }
        double saldo = p.getSaldo() - t.getTOTAL();
        p.setSaldo(saldo);
        p.setDebet(p.getDebet() + t.getTOTAL());
        t.setSALDO(saldo);
        t.setPAYDATE(new Timestamp(System.currentTimeMillis()));
        session.update(p);
        session.getTransaction().commit();
        session.getTransaction().begin();
        TransaksiDeposit transaksiDeposit = new TransaksiDeposit();
        transaksiDeposit.setDebet(t.getTOTAL());
        transaksiDeposit.setSaldo(saldo);
        transaksiDeposit.setPartner_id(t.getPARTNER_ID());
        transaksiDeposit.setDescription("Payment " + t.getNTRANS());
        transaksiDeposit.setKredit(0);
        session.save(transaksiDeposit);
        session.getTransaction().commit();
        session.getTransaction().begin();
        session.update(t);
        session.getTransaction().commit();
        session.close();
        return true;
    }


    public PartnerDeposit findPartnerDeposit(String partner_id) {
        Session session = SessionUtils.getSession();
        PartnerDeposit partnerDeposit = (PartnerDeposit) session.get(PartnerDeposit.class,partner_id);
        session.close();
        return partnerDeposit;
    }

    public void reverse(Transaction t, ISOMsg isoMsg) throws ISOException {
        Session session = SessionUtils.getSession();
        org.hibernate.Transaction tx = session.beginTransaction();
        String payment = null;
        if(isoMsg != null){
            payment = new String(isoMsg.pack());
        }
        PartnerDeposit p = (PartnerDeposit) session.get(PartnerDeposit.class,t.getPARTNER_ID());
        double saldo = p.getSaldo() + t.getTOTAL();
        p.setSaldo(saldo);
        p.setDebet(p.getDebet() - t.getTOTAL());
        t.setREVERSE(payment);
        t.setKREDIT(t.getTOTAL());
        t.setREVERSEDATE(new Timestamp(System.currentTimeMillis()));
        session.update(p);
        TransaksiDeposit transaksiDeposit = new TransaksiDeposit();
        transaksiDeposit.setDebet(0);
        transaksiDeposit.setSaldo(saldo);
        transaksiDeposit.setPartner_id(t.getPARTNER_ID());
        transaksiDeposit.setDescription("Reverse " + t.getNTRANS());
        transaksiDeposit.setKredit(t.getTOTAL());
        session.save(transaksiDeposit);
        t.setST(TStatus.REVERSE_PAYMENT_SUCCESS);
        session.update(t);
        tx.commit();
        session.close();
    }
}
