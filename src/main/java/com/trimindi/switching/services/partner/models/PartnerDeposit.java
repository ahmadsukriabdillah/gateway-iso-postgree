package main.java.com.trimindi.switching.services.partner.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by HP on 19/05/2017.
 */

@Entity
@Table(name = "mst_deposit")
public class PartnerDeposit {

    @Id
    private String partner_id;
    private double debet;
    private double kredit;
    private double saldo;

    public PartnerDeposit() {
    }

    public String getPartner_id() {
        return partner_id;
    }

    public PartnerDeposit setPartner_id(String partner_id) {
        this.partner_id = partner_id;
        return this;
    }

    public double getDebet() {
        return debet;
    }

    public PartnerDeposit setDebet(double debet) {
        this.debet = debet;
        return this;
    }

    public double getKredit() {
        return kredit;
    }

    public PartnerDeposit setKredit(double kredit) {
        this.kredit = kredit;
        return this;
    }

    public double getSaldo() {
        return saldo;
    }

    public PartnerDeposit setSaldo(double saldo) {
        this.saldo = saldo;
        return this;
    }
}
