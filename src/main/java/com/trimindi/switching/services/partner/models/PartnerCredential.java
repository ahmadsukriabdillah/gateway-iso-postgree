package main.java.com.trimindi.switching.services.partner.models;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.security.auth.Subject;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.security.Principal;

/**
 * Created by HP on 14/05/2017.
 */

@Entity
@Table(name = "user_id")
@XmlRootElement
@NamedNativeQueries({
        @NamedNativeQuery(
                name = "findPartnerCredentialByUID",
                query = "select * from user_id p where p.uid = :partner_uid",
                resultClass = PartnerCredential.class
        ),
        @NamedNativeQuery(
                name = "findPartnerCredentialAll",
                query = "select * from user_id",
                resultClass = PartnerCredential.class
        )
})
public class PartnerCredential implements Serializable,Principal {
    @Column(name = "partner_id")
    private String partner_id;
    @Id
    @Column(name = "uid")
    private String partner_uid;

    @Override
    public String toString() {
        return "PartnerCredential{" +
                "partner_id='" + partner_id + '\'' +
                ", partner_uid='" + partner_uid + '\'' +
                ", partner_password='" + partner_password + '\'' +
                ", partner_name='" + partner_name + '\'' +
                '}';
    }

    @Column(name = "pass")
    private String partner_password;
    @Column(name = "uid_name")
    private String partner_name;

    public PartnerCredential() {
    }

    public PartnerCredential(String partner_id, String partner_uid, String partner_password, String partner_name) {
        this.partner_id = partner_id;
        this.partner_uid = partner_uid;
        this.partner_password = partner_password;
        this.partner_name = partner_name;
    }

    public String getPartner_id() {
        return partner_id;
    }

    public void setPartner_id(String partner_id) {
        this.partner_id = partner_id;
    }

    public String getPartner_uid() {
        return partner_uid;
    }

    public void setPartner_uid(String partner_uid) {
        this.partner_uid = partner_uid;
    }


    @XmlTransient
    @JsonIgnore
    public String getPartner_password() {
        return partner_password;
    }

    public void setPartner_password(String partner_password) {
        this.partner_password = partner_password;
    }

    public String getPartner_name() {
        return partner_name;
    }

    public void setPartner_name(String partner_name) {
        this.partner_name = partner_name;
    }

    @Override
    public String getName() {
        return this.getPartner_uid() + " " + this.getPartner_password();
    }
}
