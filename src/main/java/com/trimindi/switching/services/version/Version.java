package main.java.com.trimindi.switching.services.version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Created by HP on 21/05/2017.
 */
@Entity
@Table(name = "version")
@XmlRootElement
public class Version {
    @Id
    @XmlTransient @JsonIgnore
    private int id;
    private String version;
    private String link;

    public Version() {
    }

    public int getId() {
        return id;
    }

    public Version setId(int id) {
        this.id = id;
        return this;
    }

    public String getVersion() {
        return version;
    }

    public Version setVersion(String version) {
        this.version = version;
        return this;
    }

    public String getLink() {
        return link;
    }

    public Version setLink(String link) {
        this.link = link;
        return this;
    }
}
