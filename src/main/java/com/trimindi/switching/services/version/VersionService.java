package main.java.com.trimindi.switching.services.version;

import main.java.com.trimindi.switching.services.hibernate.SessionUtils;
import org.hibernate.Session;

/**
 * Created by HP on 21/05/2017.
 */
public class VersionService {
    public VersionService() {

    }

    public Version find(){
        Session session = SessionUtils.getSession();
        int id = 1;
        Version v = (Version) session.get(Version.class,id);
        session.close();
        return v;
    }
}
