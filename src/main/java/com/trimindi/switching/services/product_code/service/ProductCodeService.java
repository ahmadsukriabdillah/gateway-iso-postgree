package main.java.com.trimindi.switching.services.product_code.service;


import main.java.com.trimindi.switching.services.hibernate.SessionUtils;
import main.java.com.trimindi.switching.services.product_code.models.ProductCode;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

/**
 * Created by sx on 13/05/17.
 */
public class ProductCodeService {

    public ProductCodeService() {
    }


    public void persist(ProductCode entity) {
        Session session = SessionUtils.getSession();
        Transaction tx = session.beginTransaction();
        session.persist(entity);
        tx.commit();
        session.close();
    }

    public void update(ProductCode entity) {
        Session session = SessionUtils.getSession();
        Transaction tx = session.beginTransaction();
        session.update(entity);
        tx.commit();
        session.close();
    }

    public ProductCode findById(Long id) {
        Session session = SessionUtils.getSession();
        ProductCode book = (ProductCode) session.get(ProductCode.class,id);
        session.close();
        return book;
    }
    public ProductCode findByDenom(String denom) {
        Session session = SessionUtils.getSession();
        ProductCode book = (ProductCode) session.getNamedQuery("findProductCodeByDenom")
                .setParameter("denom",denom).uniqueResult();
        session.close();
        return book;
    }

    public void delete(Long id) {
        Session session = SessionUtils.getSession();
        Transaction tx = session.beginTransaction();
        ProductCode pc = (ProductCode) session.get(ProductCode.class,id);
        session.delete(pc);
        tx.commit();
        session.close();
    }

    public List<ProductCode> findAll() {
        Session session = SessionUtils.getSession();
        List<ProductCode> q = session.createQuery("from ProductCode").list();
        session.close();
        return q;
    }
}
