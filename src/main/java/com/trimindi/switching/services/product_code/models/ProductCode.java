package main.java.com.trimindi.switching.services.product_code.models;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by sx on 11/05/17.
 */

@Entity
@Table(name = "mst_product_item")
@XmlRootElement
@NamedNativeQueries({
        @NamedNativeQuery(
                name = "findProductCodeByDenom",
                query = "select * from mst_product_item p where p.denom = :denom",
                resultClass = ProductCode.class
        ),
        @NamedNativeQuery(
                name = "findProductCodeAll",
                query = "select * from mst_product_item p",
                resultClass = ProductCode.class
        )
})
public class ProductCode {

    @Column(name = "prod_id")
    private String product_item;


    @Id
    @Column(name = "denom")
    private String denom;

    @Column(name = "nominal", nullable = false)
    private double nominal;

    @Column(name = "pos", nullable = false)
    private int pos;

    @Column(name = "channel", nullable = false)
    private int channel;

    public ProductCode() {
    }

    public String getProduct_item() {
        return product_item;
    }

    public ProductCode setProduct_item(String product_item) {
        this.product_item = product_item;
        return this;
    }

    public String getDenom() {
        return denom;
    }

    public ProductCode setDenom(String denom) {
        this.denom = denom;
        return this;
    }

    public double getNominal() {
        return nominal;
    }

    public ProductCode setNominal(double nominal) {
        this.nominal = nominal;
        return this;
    }

    public int isPos() {
        return pos;
    }

    public ProductCode setPos(int pos) {
        this.pos = pos;
        return this;
    }

    public int isChannel() {
        return channel;
    }

    public ProductCode setChannel(int channel) {
        this.channel = channel;
        return this;
    }
}
