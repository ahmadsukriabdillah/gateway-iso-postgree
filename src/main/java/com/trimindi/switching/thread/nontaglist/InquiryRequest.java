package main.java.com.trimindi.switching.thread.nontaglist;

import main.java.com.trimindi.switching.client.ChannelManager;
import main.java.com.trimindi.switching.response.nontaglist.InquiryResponse;
import main.java.com.trimindi.switching.services.partner.models.PartnerCredential;
import main.java.com.trimindi.switching.services.partner.models.PartnerDeposit;
import main.java.com.trimindi.switching.services.partner.models.PartnerFee;
import main.java.com.trimindi.switching.services.partner.services.PartnerDepositService;
import main.java.com.trimindi.switching.services.partner.services.PartnerFeeService;
import main.java.com.trimindi.switching.services.transaction.models.Transaction;
import main.java.com.trimindi.switching.services.transaction.service.TransactionService;
import main.java.com.trimindi.switching.thread.BaseThread;
import main.java.com.trimindi.switching.utils.constanta.Constanta;
import main.java.com.trimindi.switching.utils.constanta.ResponseCode;
import main.java.com.trimindi.switching.utils.constanta.TStatus;
import main.java.com.trimindi.switching.utils.generator.NonTagListGenerator;
import main.java.com.trimindi.switching.utils.iso.models.Rules;
import main.java.com.trimindi.switching.utils.iso.parsing.SDE;
import main.java.com.trimindi.switching.utils.rules.response.ResponseRulesGeneratorNonTagList;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.util.NameRegistrar;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.TimeoutHandler;
import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Created by HP on 15/05/2017.
 */
public class InquiryRequest extends BaseThread implements Runnable {
    private Map<String, String> params;
    private AsyncResponse response;
    private ChannelManager channelManager;
    private PartnerCredential partnerCredential;
    private PartnerDepositService partnerDepositService;
    private TransactionService transactionService;
    private PartnerFeeService partnerFeeService;

    public InquiryRequest(AsyncResponse asyncResponse, Map<String ,String> request, PartnerCredential partnerCredential) {
        super("NONTAGLIST_INQUIRY.log");
        try {
            this.partnerCredential = partnerCredential;
            this.partnerDepositService = new PartnerDepositService();
            this.transactionService = new TransactionService();
            this.partnerFeeService = new PartnerFeeService();
            this.params = request;
            this.response = asyncResponse;
            this.channelManager = (ChannelManager) NameRegistrar.get("manager");
            response.setTimeoutHandler(new TimeoutHandler() {
                @Override
                public void handleTimeout(AsyncResponse asyncResponse) {
                    sendBack(ResponseCode.SERVER_TIMEOUT);
                }
            });
            response.setTimeout(40, TimeUnit.SECONDS);
        } catch (NameRegistrar.NotFoundException e) {
            sendBack(Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(ResponseCode.SERVER_UNAVAILABLE).build());
        }
    }

    @Override
    public void run() {
        super.run();
        ISOMsg inquiry;
        try {
            inquiry = channelManager.sendMsg(NonTagListGenerator.generateInquiry(params.get(Constanta.MACHINE_TYPE),params.get(Constanta.MSSIDN)));
            if (inquiry != null) {
                if (inquiry.getString(39).equals("0000")) {
                    Transaction transaction = new Transaction();
                    boolean status = true;
                    List<Rules> bit48 = new SDE.Builder()
                            .setPayload(inquiry.getString(48))
                            .setRules(ResponseRulesGeneratorNonTagList.nonTagListInquiryResponse(48, status))
                            .generate();
                    List<Rules> bit62 = new SDE.Builder()
                            .setPayload(inquiry.getString(62))
                            .setRules(ResponseRulesGeneratorNonTagList.nonTagListInquiryResponse(62, status))
                            .generate();

                    bit48.addAll(bit62);
                    System.out.print(bit48.toString());
                    InquiryResponse inquiryResponse = new InquiryResponse(bit48,true);
                    Random random = new Random(System.currentTimeMillis());
                    random.nextInt(999999999);
                    PartnerFee partnerFee = partnerFeeService.findPartnerID(partnerCredential.getPartner_id());
                    PartnerDeposit partnerDeposit = partnerDepositService.findPartnerDeposit(partnerFee.getPartner_id());
                    String ntrans = new SimpleDateFormat("YYYYMMDDhhmmss").format(new java.util.Date()) + random.nextInt(999999999);
                    transaction
                            .setNTRANS(ntrans)
                            .setADMIN(inquiryResponse.getAdministrationCharge())
                            .setNAMA_PELANGGAN(inquiryResponse.getSubsriberName())
                            .setBUKOPINREF(inquiryResponse.getBukopinReferenceNumber())
                            .setMSSIDN(inquiryResponse.getRegistrationNumber())
                            .setINQDATE(new Timestamp(System.currentTimeMillis()))
                            .setMERCHANT_ID(params.get(Constanta.MACHINE_TYPE))
                            .setPRODUCT_CODE(params.get(Constanta.NONTAGLIST_PROCODE))
                            .setTAGIHAN(inquiryResponse.getTotalTransactionAmount())
                            .setFEE(partnerFee.getFee())
                            .setINQDATE(new Timestamp(System.currentTimeMillis()))
                            .setUSER_ID(partnerCredential.getPartner_uid())
                            .setINQUIRY(new String(inquiry.pack()))
                            .setTOTAL(inquiryResponse.getAdministrationCharge() + inquiryResponse.getTotalTransactionAmount())
                            .setDEBET(inquiryResponse.getAdministrationCharge() + inquiryResponse.getTotalTransactionAmount())
                            .setMAC(params.get(Constanta.MAC))
                            .setIPADDRESS(params.get(Constanta.IP_ADDRESS))
                            .setPARTNER_ID(partnerCredential.getPartner_id())
                            .setLATITUDE(params.get(Constanta.LATITUDE))
                            .setLONGITUDE(params.get(Constanta.LONGITUDE))
                            .setST(TStatus.INQUIRY);
                    inquiryResponse.setFee(partnerFee.getFee()).setNtrans(ntrans);
                    inquiryResponse.setSaldo(partnerDeposit.getSaldo());
                    inquiryResponse.setHarga(inquiryResponse.getAdministrationCharge() + inquiryResponse.getTotalTransactionAmount());
                    transactionService.persist(transaction);
                    sendBack(
                            Response.status(200)
                                    .entity(inquiryResponse)
                                    .build()
                    );
                } else {
                    sendBack(
                            Response.status(200)
                                    .entity(responseCode.get(inquiry.getString(39)))
                                    .build()
                    );
                }
            }
        } catch (ISOException e) {
            sendBack(Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(ResponseCode.SERVER_UNAVAILABLE).build());
        } catch (Exception e) {
            sendBack(Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(ResponseCode.SERVER_UNAVAILABLE).build());
        }
    }
    private void sendBack(ResponseCode r){
        if(response.isSuspended()){
            response.resume(Response.status(200).entity(r).build());
        }
    }
    private void sendBack(Response r){
        if(response.isSuspended()){
            response.resume(r);
        }
    }

}
