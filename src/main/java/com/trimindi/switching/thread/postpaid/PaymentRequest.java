package main.java.com.trimindi.switching.thread.postpaid;

import main.java.com.trimindi.switching.client.ChannelManager;
import main.java.com.trimindi.switching.response.postpaid.PaymentResponse;
import main.java.com.trimindi.switching.services.partner.models.PartnerDeposit;
import main.java.com.trimindi.switching.services.partner.services.PartnerDepositService;
import main.java.com.trimindi.switching.services.transaction.models.Transaction;
import main.java.com.trimindi.switching.services.transaction.service.TransactionService;
import main.java.com.trimindi.switching.thread.BaseThread;
import main.java.com.trimindi.switching.utils.constanta.ResponseCode;
import main.java.com.trimindi.switching.utils.constanta.TStatus;
import main.java.com.trimindi.switching.utils.generator.PostPaidGenerator;
import main.java.com.trimindi.switching.utils.iso.models.Rules;
import main.java.com.trimindi.switching.utils.iso.parsing.SDE;
import main.java.com.trimindi.switching.utils.rules.response.ResponseRulesGeneratorPostPaid;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.util.NameRegistrar;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.TimeoutHandler;
import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by HP on 15/05/2017.
 */
public class PaymentRequest extends BaseThread implements Runnable {
    private AsyncResponse response;
    private ChannelManager channelManager;
    private Transaction transaction;
    private TransactionService transactionService;
    private PartnerDepositService partnerDepositService;

    public PaymentRequest(AsyncResponse asyncResponse, Transaction transaction) {
        try {
            this.transaction = transaction;
            this.partnerDepositService = new PartnerDepositService();
            this.transactionService = new TransactionService();
            this.response = asyncResponse;
            this.channelManager = (ChannelManager) NameRegistrar.get("manager");
            response.setTimeoutHandler(new TimeoutHandler() {
                @Override
                public void handleTimeout(AsyncResponse asyncResponse) {
                    sendBack(ResponseCode.SERVER_TIMEOUT);
                }
            });
            response.setTimeout(40, TimeUnit.SECONDS);
        } catch (NameRegistrar.NotFoundException e) {
            sendBack(ResponseCode.SERVER_UNAVAILABLE);
        }
    }

    @Override
    public void run() {
        super.run();
        List<Rules> rule;
        ISOMsg payment = null;
        try {
            ISOMsg paymentRequest = PostPaidGenerator.generatePurchase(transaction);
            payment = channelManager.sendMsg(paymentRequest);
            if (payment != null) {
                if(payment.getString(39).equals("0000")){
                    boolean status = true;
                    rule = parsingRules(payment, status);
                    PaymentResponse paymentResponse = new PaymentResponse(rule);
                    transaction.setPAYMENT(new String(payment.pack())).setPAYDATE(new Timestamp(System.currentTimeMillis()))
                            .setST(TStatus.PAYMENT_SUCCESS)
                            .setBUKOPINREF(paymentResponse.getBukopinReferenceNumber());
                    transactionService.update(transaction);
                    PartnerDeposit partnerDeposit = partnerDepositService.findPartnerDeposit(transaction.getPARTNER_ID());
                    paymentResponse.setFee(transaction.getFEE());
                    paymentResponse.setSaldo(partnerDeposit.getSaldo());
                    sendBack(
                            Response.status(200)
                                    .entity(paymentResponse)
                                    .build()
                    );
                }else{
                    if (failedCode.containsKey(payment.getString(39))) {
                        payment = null;
                        ISOMsg reversal = PostPaidGenerator.generateReversal(paymentRequest, "2400");
                        payment = channelManager.sendMsg(reversal);
                        if (payment != null) {
                            if (payment.getString(39).equals("0000")) {
                                partnerDepositService.reverse(transaction, payment);
                                sendBack(ResponseCode.PAYMENT_FAILED);
                            } else if (failedCode.containsKey(payment.getString(39))) {
                                reversal = PostPaidGenerator.generateReversal(paymentRequest, "2401");
                                payment = null;
                                payment = channelManager.sendMsg(reversal);
                                if (payment != null) {
                                    if (payment.getString(39).equals("0000")) {
                                        partnerDepositService.reverse(transaction, payment);
                                        sendBack(ResponseCode.PAYMENT_FAILED);
                                    } else if(failedCode.containsKey(payment.getString(39))) {
                                        reversal = PostPaidGenerator.generateReversal(paymentRequest, "2401");
                                        payment = null;
                                        payment = channelManager.sendMsg(reversal);
                                        partnerDepositService.reverse(transaction, payment);
                                        sendBack(ResponseCode.PAYMENT_FAILED);
                                    }else{
                                        partnerDepositService.reverse(transaction, payment);
                                        sendBack(ResponseCode.PAYMENT_FAILED);
                                    }
                                }else{
                                    partnerDepositService.reverse(transaction, payment);
                                    sendBack(ResponseCode.PAYMENT_FAILED);
                                }
                            } else {
                                partnerDepositService.reverse(transaction, payment);
                                sendBack(ResponseCode.PAYMENT_FAILED);
                            }
                        }else{
                            partnerDepositService.reverse(transaction, payment);
                            sendBack(ResponseCode.PAYMENT_FAILED);
                        }

                    } else {
                        partnerDepositService.reverse(transaction, payment);
                        sendBack(
                                Response.status(200)
                                        .entity(responseCode.get(payment.getString(39)))
                                        .build()
                        );
                    }
                }
            } else {
                payment = null;
                ISOMsg reversal = PostPaidGenerator.generateReversal(paymentRequest, "2400");
                payment = channelManager.sendMsg(reversal);
                if (payment != null) {
                    if (payment.getString(39).equals("0000")) {
                        partnerDepositService.reverse(transaction, payment);
                        sendBack(ResponseCode.PAYMENT_FAILED);
                    } else if (failedCode.containsKey(payment.getString(39))) {
                        reversal = PostPaidGenerator.generateReversal(paymentRequest, "2401");
                        payment = null;
                        payment = channelManager.sendMsg(reversal);
                        if (payment != null) {
                            if (payment.getString(39).equals("0000")) {
                                partnerDepositService.reverse(transaction, payment);
                                sendBack(ResponseCode.PAYMENT_FAILED);
                            } else if(failedCode.containsKey(payment.getString(39))) {
                                reversal = PostPaidGenerator.generateReversal(paymentRequest, "2401");
                                payment = null;
                                payment = channelManager.sendMsg(reversal);
                                partnerDepositService.reverse(transaction, payment);
                                sendBack(ResponseCode.PAYMENT_FAILED);
                            } else {
                                partnerDepositService.reverse(transaction, payment);
                                sendBack(ResponseCode.PAYMENT_FAILED);
                            }
                        }else{
                            partnerDepositService.reverse(transaction, payment);
                            sendBack(ResponseCode.PAYMENT_FAILED);
                        }
                    } else {
                        partnerDepositService.reverse(transaction, payment);
                        sendBack(ResponseCode.PAYMENT_FAILED);
                    }
                }else{
                    reversal = PostPaidGenerator.generateReversal(paymentRequest, "2401");
                    payment = null;
                    payment = channelManager.sendMsg(reversal);
                    if (payment != null) {
                        if (payment.getString(39).equals("0000")) {
                            partnerDepositService.reverse(transaction, payment);
                            sendBack(ResponseCode.PAYMENT_FAILED);
                        } else if (failedCode.containsKey(payment.getString(39))) {
                            reversal = PostPaidGenerator.generateReversal(paymentRequest, "2401");
                            payment = null;
                            payment = channelManager.sendMsg(reversal);
                            partnerDepositService.reverse(transaction, payment);
                            sendBack(ResponseCode.PAYMENT_FAILED);
                        } else {
                            partnerDepositService.reverse(transaction, payment);
                            sendBack(ResponseCode.PAYMENT_FAILED);
                        }
                    }else{
                        partnerDepositService.reverse(transaction, payment);
                        sendBack(ResponseCode.PAYMENT_FAILED);
                    }
                }
            }
        } catch (ISOException e) {
            try {
                partnerDepositService.reverse(transaction, payment);
            } catch (ISOException e1) {
                e1.printStackTrace();
            }
            sendBack(ResponseCode.SERVER_UNAVAILABLE);
        } catch (Exception e) {
            try {
                partnerDepositService.reverse(transaction, payment);
            } catch (ISOException e1) {
                e1.printStackTrace();
            }
            sendBack(ResponseCode.SERVER_UNAVAILABLE);
        }

    }

    private List<Rules> parsingRules(ISOMsg d, boolean status) {
        List<Rules> bit48 = new SDE.Builder()
                .setPayload(d.getString(48))
                .setRules(ResponseRulesGeneratorPostPaid.postPaidPaymentResponse(48, status))
                .generate();
        bit48.add(new Rules(d.getString(63)));
        return bit48;
    }

    private void sendBack(ResponseCode r){
        if(response.isSuspended()){
            response.resume(Response.status(200).entity(r).build());
        }
    }
    private void sendBack(Response r){
        if(response.isSuspended()){
            response.resume(r);
        }
    }
}
