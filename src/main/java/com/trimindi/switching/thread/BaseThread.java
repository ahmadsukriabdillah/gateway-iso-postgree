package main.java.com.trimindi.switching.thread;

import main.java.com.trimindi.switching.services.transaction.service.TransactionService;
import main.java.com.trimindi.switching.utils.TLog;
import main.java.com.trimindi.switching.utils.constanta.ResponseCode;

import javax.xml.ws.Response;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by HP on 15/05/2017.
 */
public class BaseThread extends Thread {
    protected TLog log;
    protected TransactionService transactionService;

    public BaseThread(String filename) {
        log = new TLog(filename);
        transactionService = new TransactionService();
    }

    public BaseThread(){
    }

    public static final Map<String,ResponseCode> failedCode;
    public static final Map<String, ResponseCode> responseCode;
    static {
        Map<String, ResponseCode> aMap = new HashMap<>();
        aMap.put("0000", new ResponseCode("0000", "Successful"));
        aMap.put("0004", new ResponseCode("0004", "ERROR - Unregistered biller"));
        aMap.put("0005", new ResponseCode("0005", "ERROR - Other"));
        aMap.put("0006", new ResponseCode("0006", "ERROR - Blocked Partner Central"));
        aMap.put("0007", new ResponseCode("0007", "ERROR - Blocked Terminal"));
        aMap.put("0008", new ResponseCode("0008", "ERROR - Invalid access-time"));
        aMap.put("0009", new ResponseCode("0009", "ERROR - Inactive Account"));
        aMap.put("0011", new ResponseCode("0011", "ERROR - Need to sign-on"));
        aMap.put("0012", new ResponseCode("0012", "ERROR - Cannot reverse because exceed time limit"));
        aMap.put("0013", new ResponseCode("0013", "ERROR - Invalid Transaction Amount"));
        aMap.put("0014", new ResponseCode("0014", "ERROR - Unregistered subscriber number/ID"));
        aMap.put("0015", new ResponseCode("0015", "ERROR - Unknown Meter SN"));
        aMap.put("0016", new ResponseCode("0016", "ERROR - PRR Subscriber"));
        aMap.put("0017", new ResponseCode("0017", "ERROR - Subscriber has outstanding bills"));
        aMap.put("0026", new ResponseCode("0026", "ERROR - Insufficient quota"));
        aMap.put("0030", new ResponseCode("0030", "ERROR - Invalid message"));
        aMap.put("0031", new ResponseCode("0031", "ERROR - Unregistered Bank Code"));
        aMap.put("0032", new ResponseCode("0032", "ERROR - Unregistered Partner Central"));
        aMap.put("0033", new ResponseCode("0033", "ERROR - Unregistered Product"));
        aMap.put("0034", new ResponseCode("0034", "ERROR - Unregistered terminal"));
        aMap.put("0041", new ResponseCode("0041", "ERROR - Transaction amount below minimum purchase amount"));
        aMap.put("0042", new ResponseCode("0042", "ERROR - Transaction amount abovemaximum purchase amount"));
        aMap.put("0043", new ResponseCode("0043", "ERROR - New power category iso smaller"));
        aMap.put("0044", new ResponseCode("0044", "ERROR - New power category iso not a valid value"));
        aMap.put("0045", new ResponseCode("0045", "ERROR - Invalid admin charges"));
        aMap.put("0046", new ResponseCode("0046", "ERROR - Insufficient deposit"));
        aMap.put("0047", new ResponseCode("0047", "ERROR - Total KWH iso over the limit"));
        aMap.put("0048", new ResponseCode("0048", "ERROR - Registration iso expired"));
        aMap.put("0063", new ResponseCode("0063", "ERROR - No payment"));
        aMap.put("0068", new ResponseCode("0068", "ERROR - Timeout"));
        aMap.put("0077", new ResponseCode("0077", "ERROR - Subscriber suspended"));
        aMap.put("0088", new ResponseCode("0088", "ERROR - Bills already paid"));
        aMap.put("0089", new ResponseCode("0089", "ERROR - Current bill iso not available"));
        aMap.put("0090", new ResponseCode("0090", "ERROR - Cut-off iso in progress"));
        aMap.put("0091", new ResponseCode("0091", "ERROR - Database"));
        aMap.put("0092", new ResponseCode("0092", "ERROR - Switcher receipt reference number iso not available"));
        aMap.put("0093", new ResponseCode("0093", "ERROR - Invalid switcher reference number"));
        aMap.put("0094", new ResponseCode("0094", "ERROR - Reversal had been done"));
        aMap.put("0095", new ResponseCode("0095", "ERROR - Unregistered Merchant type"));
        aMap.put("0096", new ResponseCode("0096", "ERROR - Transaction was not found on Legacy System"));
        aMap.put("0097", new ResponseCode("0097", "ERROR - Switching ID and/or bank code iso not identical with inquiry"));
        aMap.put("0098", new ResponseCode("0098", "ERROR - PLNreference number iso not valid"));
        responseCode = Collections.unmodifiableMap(aMap);


        Map<String, ResponseCode> aMap2 = new HashMap<>();
        aMap2.put("0005", new ResponseCode("0005", "ERROR - Other"));
        aMap2.put("0063", new ResponseCode("0063", "ERROR - No payment"));
        aMap2.put("0068", new ResponseCode("0068", "ERROR - Timeout"));
        failedCode = Collections.unmodifiableMap(aMap2);
    }


}
