package main.java.com.trimindi.switching.thread.prepaid;

import main.java.com.trimindi.switching.client.ChannelManager;
import main.java.com.trimindi.switching.response.prepaid.PaymentResponse;
import main.java.com.trimindi.switching.services.partner.models.PartnerDeposit;
import main.java.com.trimindi.switching.services.partner.services.PartnerDepositService;
import main.java.com.trimindi.switching.services.transaction.models.Transaction;
import main.java.com.trimindi.switching.services.transaction.service.TransactionService;
import main.java.com.trimindi.switching.thread.BaseThread;
import main.java.com.trimindi.switching.utils.constanta.ResponseCode;
import main.java.com.trimindi.switching.utils.constanta.TStatus;
import main.java.com.trimindi.switching.utils.generator.PrePaidGenerator;
import main.java.com.trimindi.switching.utils.iso.models.Rules;
import main.java.com.trimindi.switching.utils.iso.parsing.SDE;
import main.java.com.trimindi.switching.utils.rules.response.ResponseRulesGeneratorPrePaid;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.util.NameRegistrar;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.TimeoutHandler;
import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by HP on 15/05/2017.
 */
public class PaymentRequest extends BaseThread implements Runnable {
    private Transaction transaction;
    private AsyncResponse response;
    private ChannelManager channelManager;
    private TransactionService transactionService;
    private PartnerDepositService partnerDepositService;
    private List<Rules> rule;
    private ISOMsg payment = null;
    public PaymentRequest(AsyncResponse asyncResponse, Transaction t) {
        super("PLN_PREPAID_PAYMENT.log");
        try {
            this.transaction = t;
            this.partnerDepositService = new PartnerDepositService();
            this.transactionService = new TransactionService();
            this.response = asyncResponse;
            this.channelManager = (ChannelManager) NameRegistrar.get("manager");
            response.setTimeoutHandler(new TimeoutHandler() {
                @Override
                public void handleTimeout(AsyncResponse asyncResponse) {
                    sendBack(ResponseCode.PAYMENT_UNDER_PROSES);
                }
            });
            response.setTimeout(40, TimeUnit.SECONDS);
        } catch (NameRegistrar.NotFoundException e) {
            sendBack(ResponseCode.SERVER_UNAVAILABLE);
        }
    }

    @Override
    public void run() {
        super.run();
        try {
            payment = channelManager.sendMsg(PrePaidGenerator.generatePurchase(transaction));
            if (payment != null) {
                if(payment.getString(39).equals("0000")){
                    PaymentResponse paymentResponse = transactionSuccess(payment);
                    sendBack(
                            Response.status(200)
                                    .entity(paymentResponse)
                                    .build()
                    );
                    sendBack(
                            Response.status(200)
                                    .entity(paymentResponse)
                                    .build()
                    );
                }else{
                    /**
                     * 0005
                     * 0063
                     * 0068
                     */
                    if(failedCode.containsKey(payment.getString(39))) {
                        ISOMsg advice = channelManager.sendMsg(PrePaidGenerator.generatePurchaseAdvice(transaction));
                        if(advice != null){
                            if(!advice.getString(39).equals("0000")){
                                advice = null;
                                rule = parsingRules(advice,false);
                                log.log(rule.toString());
                                advice = channelManager.sendMsg(PrePaidGenerator.generatePurchaseAdviceRepeat(transaction));
                                if(advice != null){
                                    if(!advice.getString(39).equals("0000")){
                                        rule = parsingRules(advice,false);
                                        log.log(rule.toString());
                                        partnerDepositService.reverse(transaction, advice);
                                        sendBack(
                                                Response.status(200)
                                                        .entity(ResponseCode.PAYMENT_FAILED)
                                                        .build()
                                        );
                                    }else{
                                        PaymentResponse paymentResponse = transactionSuccess(advice);
                                        sendBack(
                                                Response.status(200)
                                                        .entity(paymentResponse)
                                                        .build()
                                        );

                                    }
                                }else{
                                    partnerDepositService.reverse(transaction,advice);
                                }
                            }else{
                                PaymentResponse paymentResponse = transactionSuccess(advice);
                                sendBack(
                                        Response.status(200)
                                                .entity(paymentResponse)
                                                .build()
                                );
                            }
                        }else{
                            advice = null;
                            advice = channelManager.sendMsg(PrePaidGenerator.generatePurchaseAdviceRepeat(transaction));
                            if(advice != null) {
                                if (!advice.getString(39).equals("0000")) {
                                    rule = parsingRules(advice, false);
                                    log.log(rule.toString());
                                    partnerDepositService.reverse(transaction, advice);
                                    sendBack(
                                            Response.status(200)
                                                    .entity(ResponseCode.PAYMENT_FAILED)
                                                    .build()
                                    );
                                } else {
                                    PaymentResponse paymentResponse = transactionSuccess(advice);
                                    sendBack(
                                            Response.status(200)
                                                    .entity(paymentResponse)
                                                    .build()
                                    );
                                }
                            }
                        }

                    }else{
                        partnerDepositService.reverse(transaction, payment);
                        sendBack(Response.status(200).entity(responseCode.get(payment.getString(39))).build());
                    }
                }
            }else{
                ISOMsg advice = channelManager.sendMsg(PrePaidGenerator.generatePurchaseAdvice(transaction));
                if(advice != null){
                    if(!advice.getString(39).equals("0000")){
                        if(failedCode.containsKey(advice.getString(39)))
                        {
                            advice = null;
                            rule = parsingRules(advice,false);
                            log.log(rule.toString());
                            advice = channelManager.sendMsg(PrePaidGenerator.generatePurchaseAdviceRepeat(transaction));
                            if(advice != null){
                                if(!advice.getString(39).equals("0000")){
                                    rule = parsingRules(advice,false);
                                    log.log(rule.toString());
                                    partnerDepositService.reverse(transaction, advice);
                                    sendBack(
                                            Response.status(200)
                                                    .entity(ResponseCode.PAYMENT_FAILED)
                                                    .build()
                                    );
                                }else{
                                    PaymentResponse paymentResponse = transactionSuccess(advice);
                                    sendBack(
                                            Response.status(200)
                                                    .entity(paymentResponse)
                                                    .build()
                                    );
                                }
                            }else{
                                partnerDepositService.reverse(transaction,advice);
                            }
                        }else{
                            partnerDepositService.reverse(transaction, advice);
                            sendBack(Response.status(400).entity(responseCode.get(advice.getString(39))).build());
                        }
                    }else{
                        PaymentResponse paymentResponse = transactionSuccess(advice);
                        sendBack(
                                Response.status(200)
                                        .entity(paymentResponse)
                                        .build()
                        );
                    }
                }else{
                    advice = channelManager.sendMsg(PrePaidGenerator.generatePurchaseAdviceRepeat(transaction));
                    if(advice != null){
                        if(advice.getString(39).equals("0000")){
                            PaymentResponse paymentResponse = transactionSuccess(advice);
                            sendBack(
                                    Response.status(200)
                                            .entity(paymentResponse)
                                            .build()
                            );
                        }else if(failedCode.containsKey(advice.getString(39))){
                            partnerDepositService.reverse(transaction,advice);
                        }else{
                            partnerDepositService.reverse(transaction,advice);

                        }
                    }else{
                        advice = channelManager.sendMsg(PrePaidGenerator.generatePurchaseAdviceRepeat(transaction));
                        if(advice != null){
                            if(advice.getString(39).equals("0000")){
                                PaymentResponse paymentResponse = transactionSuccess(advice);
                                sendBack(
                                        Response.status(200)
                                                .entity(paymentResponse)
                                                .build()
                                );
                            }else{
                                partnerDepositService.reverse(transaction,advice);
                            }
                        }else{
                            partnerDepositService.reverse(transaction,advice);

                        }
                    }
                }
            }
        } catch (ISOException e) {
            try {
                partnerDepositService.reverse(transaction, payment);
            } catch (ISOException e1) {
                e1.printStackTrace();
            }
            sendBack(ResponseCode.SERVER_UNAVAILABLE);
        } catch (InterruptedException e) {
            try {
                partnerDepositService.reverse(transaction, payment);
            } catch (ISOException e1) {
                e1.printStackTrace();
            }
            sendBack(ResponseCode.SERVER_UNAVAILABLE);
        } catch (Exception e) {
            try {
                partnerDepositService.reverse(transaction, payment);
            } catch (ISOException e1) {
                e1.printStackTrace();
            }
            sendBack(ResponseCode.SERVER_UNAVAILABLE);
        }

    }

    private PaymentResponse transactionSuccess(ISOMsg msg) {
        try {
        rule = parsingRules(msg,true);
        log.log(rule.toString());
        PaymentResponse paymentResponse = new PaymentResponse(rule,true);

            transaction.setPAYDATE(new Timestamp(System.currentTimeMillis())).setPAYMENT(new String(payment.pack()))
                    .setST(TStatus.PAYMENT_SUCCESS)
                    .setTOKEN(paymentResponse.getTokenNumber())
                    .setPLNREF(paymentResponse.getPLNReferenceNumber())
                    .setBUKOPINREF(paymentResponse.getBukopinReferenceNumber());

        transactionService.update(transaction);
        PartnerDeposit partnerDeposit = partnerDepositService.findPartnerDeposit(transaction.getPARTNER_ID());
        paymentResponse.setFee(transaction.getFEE());
        paymentResponse.setSaldo(partnerDeposit.getSaldo());
        successButNotRespondToUser(transaction);
            return paymentResponse;
        } catch (ISOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void successButNotRespondToUser(Transaction transaction) {
        if(!response.isSuspended()){
            transactionService.update(transaction.setST(TStatus.SUCCESS_BUT_NOT_RESPONDED));
        }
    }

    private void sendBack(ResponseCode r){
        if(response.isSuspended()){
            response.resume(Response.status(200).entity(r).build());
        }
    }
    private void sendBack(Response r){
        if(response.isSuspended()){
            response.resume(r);
        }
    }


    private List<Rules> parsingRules(ISOMsg d,boolean status) {
        List<Rules> bit48 = new SDE.Builder()
                .setPayload(d.getString(48))
                .setRules(ResponseRulesGeneratorPrePaid.prePaidPaymentResponse(48,status))
                .generate();
        List<Rules> bit62 = new SDE.Builder()
                .setPayload(d.getString(62))
                .setRules(ResponseRulesGeneratorPrePaid.prePaidPaymentResponse(62,status))
                .generate();
        bit48.addAll(bit62);
        bit48.add(new Rules(d.getString(63)));
        return bit48;
    }
}
