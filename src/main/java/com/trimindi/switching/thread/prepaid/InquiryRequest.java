package main.java.com.trimindi.switching.thread.prepaid;

import main.java.com.trimindi.switching.client.ChannelManager;
import main.java.com.trimindi.switching.response.prepaid.InquiryResponse;
import main.java.com.trimindi.switching.services.TransactionProcess;
import main.java.com.trimindi.switching.services.partner.models.PartnerCredential;
import main.java.com.trimindi.switching.services.product_code.models.ProductCode;
import main.java.com.trimindi.switching.thread.BaseThread;
import main.java.com.trimindi.switching.utils.constanta.Constanta;
import main.java.com.trimindi.switching.utils.constanta.ResponseCode;
import main.java.com.trimindi.switching.utils.generator.PrePaidGenerator;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.util.NameRegistrar;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.TimeoutHandler;
import javax.ws.rs.core.Response;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by HP on 15/05/2017.
 */
public class InquiryRequest extends BaseThread implements Runnable {
    private Map<String, String> params;
    private AsyncResponse response;
    private ChannelManager channelManager;
    private PartnerCredential partnerCredential;
    private TransactionProcess transactionProcess;
    private ProductCode productCode;

    public InquiryRequest(AsyncResponse asyncResponse, Map<String ,String> request, PartnerCredential p, ProductCode productCode) {
        super("PLN_PREPAID_INQUIRY.log");
        try {
            this.productCode = productCode;
            this.transactionProcess = new TransactionProcess();
            this.partnerCredential = p;
            this.params = request;
            this.response = asyncResponse;
            this.channelManager = (ChannelManager) NameRegistrar.get("manager");
            response.setTimeoutHandler(new TimeoutHandler() {
                @Override
                public void handleTimeout(AsyncResponse asyncResponse) {
                    sendBack(ResponseCode.SERVER_TIMEOUT);
                }
            });
            response.setTimeout(40, TimeUnit.SECONDS);
        } catch (NameRegistrar.NotFoundException e) {
            sendBack(ResponseCode.SERVER_UNAVAILABLE);
        }
    }

    @Override
    public void run() {
        super.run();
        ISOMsg inquiry;
        try {
            inquiry = channelManager.sendMsg(PrePaidGenerator.generateInquiry(params.get(Constanta.MACHINE_TYPE), params.get(Constanta.MSSIDN)));
            if (inquiry != null) {
                if(inquiry.getString(39).equals("0000")){
                    InquiryResponse prePaidInquiryResponse = transactionProcess.inquiryPrepaid(inquiry,partnerCredential,params,productCode);
                    sendBack(Response.status(200).entity(prePaidInquiryResponse).build());
                }else{
                    sendBack(Response.status(200).entity(responseCode.get(inquiry.getString(39))).build());
                }
            }
        } catch (ISOException e) {
            sendBack(ResponseCode.SERVER_UNAVAILABLE);
        } catch (Exception e) {
            sendBack(ResponseCode.SERVER_UNAVAILABLE);
        }

    }
    private void sendBack(ResponseCode r){
        if(response.isSuspended()){
            response.resume(Response.status(200).entity(r).build());
        }
    }
    private void sendBack(Response r){
        if(response.isSuspended()){
            response.resume(r);
        }
    }


}
