package main.java.com.trimindi.switching.thread.prepaid;

import main.java.com.trimindi.switching.response.prepaid.InquiryResponse;

import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by HP on 19/05/2017.
 */
@XmlRootElement
public class ResponsInquiryRequest {
    private boolean status;

    @XmlAnyElement
    private InquiryResponse inquiryResponse;

    public ResponsInquiryRequest() {
    }
}
