package main.java.com.trimindi.switching.controllers;

import main.java.com.trimindi.switching.RestListener;
import main.java.com.trimindi.switching.services.partner.models.PartnerCredential;
import main.java.com.trimindi.switching.services.product_code.models.ProductCode;
import main.java.com.trimindi.switching.services.transaction.models.Transaction;
import main.java.com.trimindi.switching.thread.prepaid.InquiryRequest;
import main.java.com.trimindi.switching.thread.prepaid.PaymentRequest;
import main.java.com.trimindi.switching.utils.constanta.Constanta;

import javax.ws.rs.core.Response;
import main.java.com.trimindi.switching.utils.constanta.ResponseCode;
import main.java.com.trimindi.switching.utils.constanta.TStatus;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.iso.ISOPackager;
import org.jpos.util.LogSource;

import javax.ws.rs.*;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sx on 12/05/17.
 */
@Path("/pln/prepaid")
public class PrepaidControllers extends RestListener implements LogSource, Configurable {

    public PrepaidControllers(ISOPackager packager) {
        super(packager);
    }

    @Override
    public void setConfiguration(Configuration configuration) throws ConfigurationException {

    }

    @POST
    @Path("inquiry")
    @Consumes({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public void sendISO(@Suspended final AsyncResponse asyncResponse,
                        @Context ContainerRequestContext security,
                        @DefaultValue("") @QueryParam("denom") String denom,
                        @DefaultValue("") @QueryParam("mssidn") final String mssidn,
                        @DefaultValue("") @QueryParam("mt") String mt) {
        PartnerCredential p = (PartnerCredential) security.getProperty(Constanta.PRINCIPAL);
        if(mssidn.isEmpty()  || mssidn.length() > 12){
            asyncResponse.resume(Response.status(200).entity(ResponseCode.MISSING_REQUIRED_PARAMETER).build());
            return;
        }
        if(mt.isEmpty()  || mt.length() > 4){
            asyncResponse.resume(Response.status(200).entity(ResponseCode.MISSING_REQUIRED_PARAMETER).build());
            return;

        }

        if(denom.isEmpty()){
            asyncResponse.resume(Response.status(200).entity(ResponseCode.MISSING_REQUIRED_PARAMETER).build());
            return;
        }


        if(!merchantService.isAvailabel(mt)){
            asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_MERCHANT_TYPE).build());
            return;
        }

        ProductCode productCode = productCodeService.findByDenom(denom);
        if(productCode == null){
            asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_UNKNOW_DENOM).build());
            return;
        }

        Transaction transaction = transactionService.findByMSSIDN(mssidn);
        if(transaction != null){
            if(transaction.getST().equals(TStatus.PAYMENT_PROSESS)){
                asyncResponse.resume(Response.status(200).entity(ResponseCode.PAYMENT_UNDER_PROSES).build());
                return;
            }
        }
        Map<String, String> params = new HashMap<>();
        params.put(Constanta.MACHINE_TYPE,mt);
        params.put(Constanta.MSSIDN,mssidn);
        params.put(Constanta.DENOM,productCode.getDenom());
        params.put(Constanta.IP_ADDRESS, String.valueOf(security.getProperty(Constanta.IP_ADDRESS)));
        params.put(Constanta.MAC, String.valueOf(security.getProperty(Constanta.MAC)));
        params.put(Constanta.LONGITUDE, String.valueOf(security.getProperty(Constanta.LONGITUDE)));
        params.put(Constanta.LATITUDE, String.valueOf(security.getProperty(Constanta.LATITUDE)));
        new InquiryRequest(asyncResponse,params,p,productCode).start();
    }

    @POST
    @Path("payment")
    @Consumes({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
    @Produces({ MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML })
    public void payment(@Suspended final AsyncResponse asyncResponse,
                        @Context ContainerRequestContext security,
                        @DefaultValue("")@QueryParam("mt") final String mt,
                        @DefaultValue("") @QueryParam("ntrans") String ntrans) {

        PartnerCredential p = (PartnerCredential) security.getProperty(Constanta.PRINCIPAL);

        if(mt.isEmpty()  || mt.length() > 4){
            asyncResponse.resume(Response.status(200).entity(ResponseCode.MISSING_REQUIRED_PARAMETER).build());
            return;

        }

        Transaction transaction = transactionService.findById(ntrans);
        if(transaction == null){
            asyncResponse.resume(Response.status(200).entity(ResponseCode.NTRANS_NOT_FOUND).build());
            return;
        }

        if(!merchantService.isAvailabel(mt)){
            asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_MERCHANT_TYPE).build());
            return;
        }

        if(transaction.getST().equals(TStatus.PAYMENT_PROSESS)){
            asyncResponse.resume(Response.status(200).entity(ResponseCode.PAYMENT_UNDER_PROSES).build());
            return;
        }
        if(transaction.getST().equals(TStatus.INQUIRY)){
            if(partnerDepositService.bookingSaldo(transaction)){
                transactionService.updateToUnderProsess(transaction);
                new PaymentRequest(asyncResponse,transaction).start();
            }else{
                if(asyncResponse.isSuspended()){
                    asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_SALDO).build());
                }
            }
        }else{
            if(asyncResponse.isSuspended()){
                asyncResponse.resume(Response.status(200).entity(ResponseCode.PAYMENT_DONE).build());
            }
        }
    }


}
