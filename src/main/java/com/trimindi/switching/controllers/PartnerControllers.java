package main.java.com.trimindi.switching.controllers;

import main.java.com.trimindi.switching.ResponseLogin;
import main.java.com.trimindi.switching.RestListener;
import main.java.com.trimindi.switching.response.PartnerDepositResponse;
import main.java.com.trimindi.switching.response.ReportResponse;
import main.java.com.trimindi.switching.packager.BukopinPackager;
import main.java.com.trimindi.switching.response.postpaid.PaymentResponse;
import main.java.com.trimindi.switching.services.partner.models.Partner;
import main.java.com.trimindi.switching.services.partner.models.PartnerCredential;
import main.java.com.trimindi.switching.services.partner.models.PartnerDeposit;
import main.java.com.trimindi.switching.services.partner.models.TransaksiDeposit;
import main.java.com.trimindi.switching.services.partner.services.PartnerCredentialService;
import main.java.com.trimindi.switching.services.partner.services.PartnerService;
import main.java.com.trimindi.switching.services.partner.services.TransaksiDepositService;
import main.java.com.trimindi.switching.services.product_code.models.ProductCode;
import main.java.com.trimindi.switching.services.transaction.models.Transaction;
import main.java.com.trimindi.switching.services.version.Version;
import main.java.com.trimindi.switching.services.version.VersionService;
import main.java.com.trimindi.switching.utils.constanta.Constanta;
import main.java.com.trimindi.switching.utils.constanta.ResponseCode;
import main.java.com.trimindi.switching.utils.constanta.TStatus;
import main.java.com.trimindi.switching.utils.iso.models.Rules;
import main.java.com.trimindi.switching.utils.iso.parsing.ParsingHelper;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOPackager;
import org.jpos.util.LogSource;

import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Objects;

/**
 * Created by sx on 12/05/17.
 */
@Path("/partner")
public class PartnerControllers extends RestListener implements LogSource, Configurable {
    private VersionService versionService;
    private PartnerService partnerService;
    private PartnerCredentialService partnerCredentialService;
    private TransaksiDepositService transaksiDepositService;
    public PartnerControllers(ISOPackager packager) {
        super(packager);
        this.versionService = new VersionService();
        this.partnerService = new PartnerService();
        this.transaksiDepositService = new TransaksiDepositService();
        this.partnerCredentialService = new PartnerCredentialService();

    }

    @Override
    public void setConfiguration(Configuration configuration) throws ConfigurationException {

    }

    @POST
    @Path("report")
    @Consumes({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response sendISO(
                        @Context ContainerRequestContext security,
                        @DefaultValue("") @QueryParam("bulan") String bulan,
                        @DefaultValue("") @QueryParam("tahun") String tahun) {
        PartnerCredential p = (PartnerCredential) security.getProperty(Constanta.PRINCIPAL);
        if(bulan.isEmpty()  || bulan.length() > 2 || tahun.isEmpty() || tahun.length() > 4){
            return Response.status(400).entity(ResponseCode.MISSING_REQUIRED_PARAMETER).build();
        }

        ReportResponse a = new ReportResponse();
        GenericEntity<ReportResponse> tran;
        List<Transaction> transaction = transactionService.findByBulanDanTahun(bulan,tahun,p.getPartner_uid());
        a.setStatus(true);
        a.setTransactionList(transaction);
        tran = new GenericEntity<ReportResponse>(a){};
        return Response.status(200).entity(tran).build();
    }

    @POST
    @Path("deposit")
    @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
    public PartnerDeposit findDeposit(@Context ContainerRequestContext security){
        PartnerCredential p = (PartnerCredential) security.getProperty(Constanta.PRINCIPAL);
        PartnerDeposit partnerDeposit = partnerDepositService.findPartnerDeposit(p.getPartner_id());
        return partnerDeposit;
    }


    @POST
    @Path("print")
    @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
    public Response print(@Context ContainerRequestContext security,
                          @QueryParam("ntrans") String ntrans,
                          @QueryParam("mt") String mt){
        PartnerCredential p = (PartnerCredential) security.getProperty(Constanta.PRINCIPAL);
        if(ntrans == null || ntrans.isEmpty() || mt == null || mt.isEmpty()){
            return Response.status(200).entity(ResponseCode.MISSING_REQUIRED_PARAMETER).build();
        }

        if(!merchantService.isAvailabel(mt)){
            return Response.status(200).entity(ResponseCode.MISSING_REQUIRED_PARAMETER).build();
        }

        Transaction transaction = transactionService.findById(ntrans);
        if(transaction == null){
            return Response.status(200).entity(ResponseCode.NTRANS_NOT_FOUND).build();
        }
        if(Objects.equals(transaction.getST(), TStatus.SUCCESS_BUT_NOT_RESPONDED) || Objects.equals(transaction.getST(), TStatus.PAYMENT_SUCCESS)){
            if(transaction.getPRT() > 3){
                return Response.status(200).entity(ResponseCode.MAXIMUM_PRINTED_RECIPT).build();
            }else{
                ISOMsg isoMsg = new ISOMsg();
                isoMsg.setPackager(new BukopinPackager());
                try {
                    isoMsg.unpack(transaction.getPAYMENT().getBytes());
                } catch (ISOException e) {
                    e.printStackTrace();
                    return Response.status(200).entity(ResponseCode.MAXIMUM_PRINTED_RECIPT).build();
                }
                List<Rules> rules;
                switch (transaction.getPRODUCT_CODE()){
                    case "99501":
                        rules = ParsingHelper.parsingRulesPostPaid(isoMsg,true);
                        PaymentResponse paymentResponse = new PaymentResponse(rules);
                        return Response.status(200).entity(paymentResponse).build();
                    case "99502":
                        rules = ParsingHelper.parsingRulesPrepaid(isoMsg,true);
                        main.java.com.trimindi.switching.response.prepaid.PaymentResponse prepaid = new main.java.com.trimindi.switching.response.prepaid.PaymentResponse(rules,true);
                        return Response.status(200).entity(prepaid).build();
                    case "99504":
                        rules = ParsingHelper.parsingRulesNontaglist(isoMsg,true);
                        main.java.com.trimindi.switching.response.nontaglist.PaymentResponse nontaglist = new main.java.com.trimindi.switching.response.nontaglist.PaymentResponse(rules);
                        return Response.status(200).entity(nontaglist).build();
                }
            }
        }
        return Response.status(200).entity(ResponseCode.NTRANS_NOT_FOUND).build();
    }

    @POST
    @Path("print/cetak")
    @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
    public Response incrementPrint(@Context ContainerRequestContext security,
                                   @QueryParam("ntrans") String ntrans,
                                   @QueryParam("mt") String mt){
        PartnerCredential p = (PartnerCredential) security.getProperty(Constanta.PRINCIPAL);
        if(ntrans == null || ntrans.isEmpty() || mt == null || mt.isEmpty()){
            return Response.status(200).entity(ResponseCode.MISSING_REQUIRED_PARAMETER).build();
        }

        if(!merchantService.isAvailabel(mt)){
            return Response.status(200).entity(ResponseCode.MISSING_REQUIRED_PARAMETER).build();
        }

        Transaction transaction = transactionService.findById(ntrans);
        if(transaction == null){
            return Response.status(200).entity(ResponseCode.NTRANS_NOT_FOUND).build();
        }
        if(Objects.equals(transaction.getST(), TStatus.SUCCESS_BUT_NOT_RESPONDED) || Objects.equals(transaction.getST(), TStatus.PAYMENT_SUCCESS)){
            if(transaction.getPRT() > 3){
                return Response.status(200).entity(ResponseCode.MAXIMUM_PRINTED_RECIPT).build();
            }else{
                transaction.setPRT(transaction.getPRT() + 1);
                transactionService.update(transaction);
                return Response.status(200).entity(new ResponseCode("1000","Success").setStatus(true)).build();
            }
        }
        return Response.status(200).entity(ResponseCode.NTRANS_NOT_FOUND).build();
    }



    @POST
    @Path("version")
    @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
    public Version findVersion(@Context ContainerRequestContext security){
        Version v = versionService.find();
        return v;
    }

    @GET
    @Path("denom")
    @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
    public Response denom(@Context ContainerRequestContext security){
        List<ProductCode> productCodes = productCodeService.findAll();

        GenericEntity<List<ProductCode>> productCodeGenericEntity = new GenericEntity<List<ProductCode>>(productCodes){};
        return Response.status(200).entity(productCodeGenericEntity).build();
    }

    @POST
    @Path("login")
    @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
    public Response checkLogin(@Context ContainerRequestContext security){
        PartnerCredential p = (PartnerCredential) security.getProperty(Constanta.PRINCIPAL);
        Partner partner = partnerService.findById(p.getPartner_id());
        PartnerDeposit partnerDeposit = partnerDepositService.findPartnerDeposit(p.getPartner_id());
        ResponseLogin partnerResponseLogin = new ResponseLogin();
        partnerResponseLogin.setData(partner).setStatus(true).setDesposit(partnerDeposit);
        return Response.status(200).entity(partnerResponseLogin).build();
    }

    @POST
    @Path("chpass")
    @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
    public Response chpass(@Context ContainerRequestContext security,
                           @QueryParam("pass") String newpass){

        PartnerCredential p = (PartnerCredential) security.getProperty(Constanta.PRINCIPAL);
        System.out.println(p);
        p.setPartner_password(newpass);
        System.out.println(p);
        this.partnerCredentialService.update(p);
        return Response.status(200).entity(new ResponseCode("0000","ubah password berhasil").setStatus(true)).build();
    }

    @POST
    @Path("deposit/list")
    @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
    public Response listDeposit(@Context ContainerRequestContext security){
        PartnerCredential p = (PartnerCredential) security.getProperty(Constanta.PRINCIPAL);
        List<TransaksiDeposit> transaksiDeposits = transaksiDepositService.findAll(p.getPartner_id());
        PartnerDeposit partnerDeposit = partnerDepositService.findPartnerDeposit(p.getPartner_id());
        PartnerDepositResponse partnerDepositResponse = new PartnerDepositResponse();
        partnerDepositResponse.setPartnerDeposit(partnerDeposit).setTransaksiDeposits(transaksiDeposits).setStatus(true);
        return Response.status(200).entity(partnerDepositResponse).build();
    }

}
