package main.java.com.trimindi.switching.response.nontaglist;

import com.fasterxml.jackson.annotation.JsonIgnore;
import main.java.com.trimindi.switching.utils.generator.BaseHelper;
import main.java.com.trimindi.switching.utils.iso.models.Rules;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.List;

/**
 * Created by HP on 18/05/2017.
 */

@XmlRootElement
public class PaymentResponse extends BaseHelper {
    private boolean status;
    private String SwitcherID;
    private String RegistrationNumber;
    private String AreaCode;
    private String TransactionCode;
    private String TransactionName;
    private String RegistrationDate;
    private String ExpirationDate;
    private String SubsriberID;
    private String SubsriberName;
    private String PLNReferenceNumber;
    private String BukopinReferenceNumber;
    private String ServiceUnit;
    private String ServiceUnitAddress;
    private String ServiceUnitPhone;
    private int TotalTransactionAmountMinorUnit;
    private double TotalTransactionAmount;
    private int PLNBILLMinorUnit;
    private double PLNBILLValue;
    private int AdministrationChargeMinorUnit;
    private double AdministrationCharge;
    /**
     * 62
     */
    private String MutationNumber;
    private String SubscriberSegmentation;
    private int PowerConsumingCategory;
    private String BukopinReference;

    /**
     * 62
     */
    private String Billcomponenttype;
    private int BillComponentMinorUnit;
    private double BillComponentValueAmount;
    private double fee;
    private double saldo;
    /**
     * 63
     */

    private String message;

    public PaymentResponse() {
    }

    public PaymentResponse(List<Rules> rules) {
        this.status = true;
        this.SwitcherID = (String) rules.get(0).getResults();
        this.RegistrationNumber = (String) rules.get(1).getResults();
        this.AreaCode = (String) rules.get(2).getResults();
        this.TransactionCode = (String) rules.get(3).getResults();
        this.TransactionName = (String) rules.get(4).getResults();
        this.RegistrationDate = (String) rules.get(5).getResults();
        this.ExpirationDate = (String) rules.get(6).getResults();
        this.SubsriberID = (String) rules.get(7).getResults();
        this.SubsriberName = (String) rules.get(8).getResults();
        this.PLNReferenceNumber = (String) rules.get(9).getResults();
        this.BukopinReferenceNumber = (String) rules.get(10).getResults();
        this.ServiceUnit = (String) rules.get(11).getResults();
        this.ServiceUnitAddress = (String) rules.get(12).getResults();
        this.ServiceUnitPhone = (String) rules.get(13).getResults();
        System.out.println("TotalTransactionAmountMinorUnit" + rules.get(14).getResults());
        this.TotalTransactionAmountMinorUnit = Integer.parseInt((String) rules.get(14).getResults());
        this.TotalTransactionAmount = numberMinorUnit((String) rules.get(15).getResults(), this.TotalTransactionAmountMinorUnit);
        this.PLNBILLMinorUnit = Integer.parseInt((String) rules.get(16).getResults());
        this.PLNBILLValue = numberMinorUnit((String) rules.get(17).getResults(), this.PLNBILLMinorUnit);
        this.AdministrationChargeMinorUnit = Integer.parseInt((String) rules.get(18).getResults());
        this.AdministrationCharge = numberMinorUnit((String) rules.get(19).getResults(), this.AdministrationChargeMinorUnit);
        this.MutationNumber = (String) rules.get(20).getResults();
        this.SubscriberSegmentation = (String) rules.get(21).getResults();
        this.PowerConsumingCategory = Integer.parseInt((String) rules.get(22).getResults());
        this.BukopinReference = (String) rules.get(23).getResults();
        this.Billcomponenttype = (String) rules.get(24).getResults();
        this.BillComponentMinorUnit = Integer.parseInt((String) rules.get(25).getResults());
        this.BillComponentValueAmount = numberMinorUnit((String) rules.get(26).getResults(), this.BillComponentMinorUnit);
        this.message = (String) rules.get(27).getResults();
    }

    public boolean isStatus() {
        return status;
    }

    public PaymentResponse setStatus(boolean status) {
        this.status = status;
        return this;
    }

    public double getFee() {
        return fee;
    }

    public PaymentResponse setFee(double fee) {
        this.fee = fee;
        return this;
    }

    public double getSaldo() {
        return saldo;
    }

    public PaymentResponse setSaldo(double saldo) {
        this.saldo = saldo;
        return this;
    }

    @XmlTransient
    @JsonIgnore
    public String getSwitcherID() {
        return SwitcherID;
    }

    public PaymentResponse setSwitcherID(String switcherID) {
        SwitcherID = switcherID;
        return this;
    }

    public String getRegistrationNumber() {
        return RegistrationNumber;
    }

    public PaymentResponse setRegistrationNumber(String registrationNumber) {
        RegistrationNumber = registrationNumber;
        return this;
    }

    public String getAreaCode() {
        return AreaCode;
    }

    public PaymentResponse setAreaCode(String areaCode) {
        AreaCode = areaCode;
        return this;
    }

    @XmlTransient
    @JsonIgnore
    public String getTransactionCode() {
        return TransactionCode;
    }

    public PaymentResponse setTransactionCode(String transactionCode) {
        TransactionCode = transactionCode;
        return this;
    }

    public String getTransactionName() {
        return TransactionName;
    }

    public PaymentResponse setTransactionName(String transactionName) {
        TransactionName = transactionName;
        return this;
    }

    public String getRegistrationDate() {
        return RegistrationDate;
    }

    public PaymentResponse setRegistrationDate(String registrationDate) {
        RegistrationDate = registrationDate;
        return this;
    }

    public String getExpirationDate() {
        return ExpirationDate;
    }

    public PaymentResponse setExpirationDate(String expirationDate) {
        ExpirationDate = expirationDate;
        return this;
    }

    public String getSubsriberID() {
        return SubsriberID;
    }

    public PaymentResponse setSubsriberID(String subsriberID) {
        SubsriberID = subsriberID;
        return this;
    }

    public String getSubsriberName() {
        return SubsriberName;
    }

    public PaymentResponse setSubsriberName(String subsriberName) {
        SubsriberName = subsriberName;
        return this;
    }

    public String getPLNReferenceNumber() {
        return PLNReferenceNumber;
    }

    public PaymentResponse setPLNReferenceNumber(String PLNReferenceNumber) {
        this.PLNReferenceNumber = PLNReferenceNumber;
        return this;
    }

    public String getBukopinReferenceNumber() {
        return BukopinReferenceNumber;
    }

    public PaymentResponse setBukopinReferenceNumber(String bukopinReferenceNumber) {
        BukopinReferenceNumber = bukopinReferenceNumber;
        return this;
    }

    @XmlTransient
    @JsonIgnore
    public String getServiceUnit() {
        return ServiceUnit;
    }

    public PaymentResponse setServiceUnit(String serviceUnit) {
        ServiceUnit = serviceUnit;
        return this;
    }


    public String getServiceUnitAddress() {
        return ServiceUnitAddress;
    }

    public PaymentResponse setServiceUnitAddress(String serviceUnitAddress) {
        ServiceUnitAddress = serviceUnitAddress;
        return this;
    }

    public String getServiceUnitPhone() {
        return ServiceUnitPhone;
    }

    public PaymentResponse setServiceUnitPhone(String serviceUnitPhone) {
        ServiceUnitPhone = serviceUnitPhone;
        return this;
    }

    @XmlTransient
    @JsonIgnore
    public int getTotalTransactionAmountMinorUnit() {
        return TotalTransactionAmountMinorUnit;
    }

    public PaymentResponse setTotalTransactionAmountMinorUnit(int totalTransactionAmountMinorUnit) {
        TotalTransactionAmountMinorUnit = totalTransactionAmountMinorUnit;
        return this;
    }


    public double getTotalTransactionAmount() {
        return TotalTransactionAmount;
    }

    public PaymentResponse setTotalTransactionAmount(double totalTransactionAmount) {
        TotalTransactionAmount = totalTransactionAmount;
        return this;
    }

    @XmlTransient
    @JsonIgnore
    public int getPLNBILLMinorUnit() {
        return PLNBILLMinorUnit;
    }

    public PaymentResponse setPLNBILLMinorUnit(int PLNBILLMinorUnit) {
        this.PLNBILLMinorUnit = PLNBILLMinorUnit;
        return this;
    }

    public double getPLNBILLValue() {
        return PLNBILLValue;
    }

    public PaymentResponse setPLNBILLValue(double PLNBILLValue) {
        this.PLNBILLValue = PLNBILLValue;
        return this;
    }

    @XmlTransient
    @JsonIgnore
    public int getAdministrationChargeMinorUnit() {
        return AdministrationChargeMinorUnit;
    }

    public PaymentResponse setAdministrationChargeMinorUnit(int administrationChargeMinorUnit) {
        AdministrationChargeMinorUnit = administrationChargeMinorUnit;
        return this;
    }

    public double getAdministrationCharge() {
        return AdministrationCharge;
    }

    public PaymentResponse setAdministrationCharge(double administrationCharge) {
        AdministrationCharge = administrationCharge;
        return this;
    }

    public String getMutationNumber() {
        return MutationNumber;
    }

    public PaymentResponse setMutationNumber(String mutationNumber) {
        MutationNumber = mutationNumber;
        return this;
    }

    public String getSubscriberSegmentation() {
        return SubscriberSegmentation;
    }

    public PaymentResponse setSubscriberSegmentation(String subscriberSegmentation) {
        SubscriberSegmentation = subscriberSegmentation;
        return this;
    }

    public int getPowerConsumingCategory() {
        return PowerConsumingCategory;
    }

    public PaymentResponse setPowerConsumingCategory(int powerConsumingCategory) {
        PowerConsumingCategory = powerConsumingCategory;
        return this;
    }

    @XmlTransient
    @JsonIgnore
    public String getBukopinReference() {
        return BukopinReference;
    }

    public PaymentResponse setBukopinReference(String bukopinReference) {
        BukopinReference = bukopinReference;
        return this;
    }

    @XmlTransient
    @JsonIgnore
    public String getBillcomponenttype() {
        return Billcomponenttype;
    }

    public PaymentResponse setBillcomponenttype(String billcomponenttype) {
        Billcomponenttype = billcomponenttype;
        return this;
    }

    @XmlTransient
    @JsonIgnore
    public int getBillComponentMinorUnit() {
        return BillComponentMinorUnit;
    }

    public PaymentResponse setBillComponentMinorUnit(int billComponentMinorUnit) {
        BillComponentMinorUnit = billComponentMinorUnit;
        return this;
    }

    public double getBillComponentValueAmount() {
        return BillComponentValueAmount;
    }

    public PaymentResponse setBillComponentValueAmount(double billComponentValueAmount) {
        BillComponentValueAmount = billComponentValueAmount;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public PaymentResponse setMessage(String message) {
        this.message = message;
        return this;
    }
}
