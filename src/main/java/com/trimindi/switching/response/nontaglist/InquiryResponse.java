package main.java.com.trimindi.switching.response.nontaglist;

import com.fasterxml.jackson.annotation.JsonIgnore;
import main.java.com.trimindi.switching.utils.generator.BaseHelper;
import main.java.com.trimindi.switching.utils.iso.models.Rules;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.List;

/**
 * Created by HP on 18/05/2017.
 */
@XmlRootElement
public class InquiryResponse extends BaseHelper {
    private boolean status;
    private String SwitcherID;
    private String RegistrationNumber;
    private String AreaCode;
    private String TransactionCode;
    private String TransactionName;
    private String RegistrationDate;
    private String ExpirationDate;
    private String SubsriberID;
    private String SubsriberName;
    private String PLNReferenceNumber;
    private String BukopinReferenceNumber;
    private String ServiceUnit;
    private String ServiceUnitAddress;
    private String ServiceUnitPhone;
    private int TotalTransactionAmountMinorUnit;
    private double TotalTransactionAmount;
    private int PLNBILLMinorUnit;
    private double PLNBILLValue;
    private int AdministrationChargeMinorUnit;
    private double AdministrationCharge;
    private double fee;
    private String ntrans;
    private double harga;
    private double saldo;

    public double getFee() {
        return fee;
    }

    public InquiryResponse setFee(double fee) {
        this.fee = fee;
        return this;
    }

    public String getNtrans() {
        return ntrans;
    }

    public InquiryResponse setNtrans(String ntrans) {
        this.ntrans = ntrans;
        return this;
    }

    public double getHarga() {
        return harga;
    }

    public InquiryResponse setHarga(double harga) {
        this.harga = harga;
        return this;
    }

    public double getSaldo() {
        return saldo;
    }

    public InquiryResponse setSaldo(double saldo) {
        this.saldo = saldo;
        return this;
    }

    /**
     * 62
     */

    private String Billcomponenttype;
    private int BillComponentMinorUnit;
    private double BillComponentValueAmount;

    public InquiryResponse(List<Rules> rules,boolean status) {
        this.status = status;
        this.SwitcherID = (String) rules.get(0).getResults();
        this.RegistrationNumber = (String) rules.get(1).getResults();
        this.AreaCode = (String) rules.get(2).getResults();
        this.TransactionCode = (String) rules.get(3).getResults();
        this.TransactionName = (String) rules.get(4).getResults();
        this.RegistrationDate = (String) rules.get(5).getResults();
        this.ExpirationDate = (String) rules.get(6).getResults();
        this.SubsriberID = (String) rules.get(7).getResults();
        this.SubsriberName = (String) rules.get(8).getResults();
        this.PLNReferenceNumber = (String) rules.get(9).getResults();
        this.BukopinReferenceNumber = (String) rules.get(10).getResults();
        this.ServiceUnit = (String) rules.get(11).getResults();
        this.ServiceUnitAddress = (String) rules.get(12).getResults();
        this.ServiceUnitPhone = (String) rules.get(13).getResults();
        this.TotalTransactionAmountMinorUnit = Integer.parseInt((String) rules.get(14).getResults());
        this.TotalTransactionAmount = numberMinorUnit((String) rules.get(15).getResults(),this.TotalTransactionAmountMinorUnit);
        this.PLNBILLMinorUnit = Integer.parseInt((String) rules.get(16).getResults());
        this.PLNBILLValue = numberMinorUnit((String) rules.get(17).getResults(),this.PLNBILLMinorUnit);
        this.AdministrationChargeMinorUnit = Integer.parseInt((String) rules.get(18).getResults());
        this.AdministrationCharge = numberMinorUnit((String) rules.get(19).getResults(),this.AdministrationChargeMinorUnit);
        this.Billcomponenttype = (String) rules.get(20).getResults();
        this.BillComponentMinorUnit = Integer.parseInt((String) rules.get(21).getResults());
        this.BillComponentValueAmount = numberMinorUnit((String) rules.get(22).getResults(),this.BillComponentMinorUnit);
    }

    public InquiryResponse() {
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @XmlTransient @JsonIgnore
    public String getSwitcherID() {
        return SwitcherID;
    }

    public void setSwitcherID(String switcherID) {
        SwitcherID = switcherID;
    }

    public String getRegistrationNumber() {
        return RegistrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        RegistrationNumber = registrationNumber;
    }

    public String getAreaCode() {
        return AreaCode;
    }

    public void setAreaCode(String areaCode) {
        AreaCode = areaCode;
    }

    public String getTransactionCode() {
        return TransactionCode;
    }

    public void setTransactionCode(String transactionCode) {
        TransactionCode = transactionCode;
    }

    public String getTransactionName() {
        return TransactionName;
    }

    public void setTransactionName(String transactionName) {
        TransactionName = transactionName;
    }

    public String getRegistrationDate() {
        return RegistrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        RegistrationDate = registrationDate;
    }

    public String getExpirationDate() {
        return ExpirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        ExpirationDate = expirationDate;
    }

    public String getSubsriberID() {
        return SubsriberID;
    }

    public void setSubsriberID(String subsriberID) {
        SubsriberID = subsriberID;
    }

    public String getSubsriberName() {
        return SubsriberName;
    }

    public void setSubsriberName(String subsriberName) {
        SubsriberName = subsriberName;
    }

    @XmlTransient @JsonIgnore
    public String getPLNReferenceNumber() {
        return PLNReferenceNumber;
    }

    public void setPLNReferenceNumber(String PLNReferenceNumber) {
        this.PLNReferenceNumber = PLNReferenceNumber;
    }

    @XmlTransient @JsonIgnore
    public String getBukopinReferenceNumber() {
        return BukopinReferenceNumber;
    }

    public void setBukopinReferenceNumber(String bukopinReferenceNumber) {
        BukopinReferenceNumber = bukopinReferenceNumber;
    }

    @XmlTransient @JsonIgnore
    public String getServiceUnit() {
        return ServiceUnit;
    }

    public void setServiceUnit(String serviceUnit) {
        ServiceUnit = serviceUnit;
    }

    @XmlTransient @JsonIgnore
    public String getServiceUnitAddress() {
        return ServiceUnitAddress;
    }

    public void setServiceUnitAddress(String serviceUnitAddress) {
        ServiceUnitAddress = serviceUnitAddress;
    }

    @XmlTransient @JsonIgnore
    public String getServiceUnitPhone() {
        return ServiceUnitPhone;
    }

    public void setServiceUnitPhone(String serviceUnitPhone) {
        ServiceUnitPhone = serviceUnitPhone;
    }

    @XmlTransient @JsonIgnore
    public int getTotalTransactionAmountMinorUnit() {
        return TotalTransactionAmountMinorUnit;
    }

    public void setTotalTransactionAmountMinorUnit(int totalTransactionAmountMinorUnit) {
        TotalTransactionAmountMinorUnit = totalTransactionAmountMinorUnit;
    }

    public double getTotalTransactionAmount() {
        return TotalTransactionAmount;
    }

    public void setTotalTransactionAmount(double totalTransactionAmount) {
        TotalTransactionAmount = totalTransactionAmount;
    }

    @XmlTransient @JsonIgnore
    public int getPLNBILLMinorUnit() {
        return PLNBILLMinorUnit;
    }

    public void setPLNBILLMinorUnit(int PLNBILLMinorUnit) {
        this.PLNBILLMinorUnit = PLNBILLMinorUnit;
    }

    public double getPLNBILLValue() {
        return PLNBILLValue;
    }

    public void setPLNBILLValue(double PLNBILLValue) {
        this.PLNBILLValue = PLNBILLValue;
    }

    @XmlTransient @JsonIgnore
    public int getAdministrationChargeMinorUnit() {
        return AdministrationChargeMinorUnit;
    }

    public void setAdministrationChargeMinorUnit(int administrationChargeMinorUnit) {
        AdministrationChargeMinorUnit = administrationChargeMinorUnit;
    }

    public double getAdministrationCharge() {
        return AdministrationCharge;
    }

    public void setAdministrationCharge(double administrationCharge) {
        AdministrationCharge = administrationCharge;
    }

    public String getBillcomponenttype() {
        return Billcomponenttype;
    }

    public void setBillcomponenttype(String billcomponenttype) {
        Billcomponenttype = billcomponenttype;
    }

    @XmlTransient @JsonIgnore
    public int getBillComponentMinorUnit() {
        return BillComponentMinorUnit;
    }

    public void setBillComponentMinorUnit(int billComponentMinorUnit) {
        BillComponentMinorUnit = billComponentMinorUnit;
    }

    public double getBillComponentValueAmount() {
        return BillComponentValueAmount;
    }

    public void setBillComponentValueAmount(double billComponentValueAmount) {
        BillComponentValueAmount = billComponentValueAmount;
    }
}
