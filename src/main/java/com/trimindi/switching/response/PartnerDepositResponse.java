package main.java.com.trimindi.switching.response;

import main.java.com.trimindi.switching.services.partner.models.PartnerDeposit;
import main.java.com.trimindi.switching.services.partner.models.TransaksiDeposit;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by HP on 22/05/2017.
 */
@XmlRootElement
public class PartnerDepositResponse {
    private boolean status;
    private PartnerDeposit partnerDeposit;
    private List<TransaksiDeposit> transaksiDeposits;

    public PartnerDepositResponse() {
    }

    public boolean isStatus() {
        return status;
    }

    public PartnerDepositResponse setStatus(boolean status) {
        this.status = status;
        return this;
    }

    public PartnerDeposit getPartnerDeposit() {
        return partnerDeposit;
    }

    public PartnerDepositResponse setPartnerDeposit(PartnerDeposit partnerDeposit) {
        this.partnerDeposit = partnerDeposit;
        return this;
    }

    public List<TransaksiDeposit> getTransaksiDeposits() {
        return transaksiDeposits;
    }

    public PartnerDepositResponse setTransaksiDeposits(List<TransaksiDeposit> transaksiDeposits) {
        this.transaksiDeposits = transaksiDeposits;
        return this;
    }
}
