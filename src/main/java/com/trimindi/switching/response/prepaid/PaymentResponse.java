package main.java.com.trimindi.switching.response.prepaid;

import com.fasterxml.jackson.annotation.JsonIgnore;
import main.java.com.trimindi.switching.utils.generator.BaseHelper;
import main.java.com.trimindi.switching.utils.iso.models.Rules;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.List;
/**
 * Created by HP on 17/05/2017.
 */


@XmlRootElement
public class PaymentResponse extends BaseHelper {

    private String Message;
    private boolean status;
    private String SwitcherID;
    private String MeterSerialNumber;
    private String SubscriberID;
    private String Flag;
    private String PLNReferenceNumber;
    private String BukopinReferenceNumber;
    private String VendingReceiptNumber;
    private String SubscriberName;
    private String SubscriberSegmentation;
    private int PowerConsumingCategory;
    private String BuyingOption;
    private int MinorUnitofAdminCharge;
    private double AdminCharge;
    private int MinorUnitofStampDuty;
    private double StampDuty;
    private int MinorUnitofValueAddedTax;
    private double ValueAddedTax;
    private int MinorUnitofPublicLightingTax;
    private double PublicLightingTax;
    private int MinorUnitofCustomerPayablesInstallment;
    private double CustomerPayablesInstallment;
    private int MinorUnitofPowerPurchase;
    private double PowerPurchase;
    private int MinorUnitofPurchasedKWHUnit;
    private int PurchasedKWHUnit;
    private String TokenNumber;
    private String DistributionCode;
    private int ServiceUnit;
    private int ServiceUnitPhone;
    private int MaxKWHLimit;
    private String TotalRepeat;
    private double fee;
    private double saldo;

    public double getFee() {
        return fee;
    }

    public PaymentResponse setFee(double fee) {
        this.fee = fee;
        return this;
    }

    public double getSaldo() {
        return saldo;
    }

    public PaymentResponse setSaldo(double saldo) {
        this.saldo = saldo;
        return this;
    }

    public PaymentResponse(List<Rules> rules, boolean status) {
        this.status = status;
        this.SwitcherID = (String) rules.get(0).getResults();
        this.MeterSerialNumber= (String) rules.get(1).getResults();
        this.SubscriberID= (String) rules.get(2).getResults();
        this.Flag= (String) rules.get(3).getResults();
        this.PLNReferenceNumber= (String) rules.get(4).getResults();
        this.BukopinReferenceNumber= (String) rules.get(5).getResults();
        this.VendingReceiptNumber= (String) rules.get(6).getResults();
        this.SubscriberName= (String) rules.get(7).getResults();
        this.SubscriberSegmentation= (String) rules.get(8).getResults();
        this.PowerConsumingCategory= Integer.parseInt((String) rules.get(9).getResults());
        this.BuyingOption= (String) rules.get(10).getResults();
        this.MinorUnitofAdminCharge= Integer.parseInt((String) rules.get(11).getResults());
        this.AdminCharge= numberMinorUnit((String) rules.get(12).getResults(),this.MinorUnitofAdminCharge);
        this.MinorUnitofStampDuty= Integer.parseInt((String) rules.get(13).getResults());
        this.StampDuty= numberMinorUnit((String) rules.get(14).getResults(),this.MinorUnitofStampDuty);
        this.MinorUnitofValueAddedTax= Integer.parseInt((String) rules.get(15).getResults());
        this.ValueAddedTax= numberMinorUnit((String) rules.get(16).getResults(),this.MinorUnitofValueAddedTax);
        this.MinorUnitofPublicLightingTax= Integer.parseInt((String) rules.get(17).getResults());
        this.PublicLightingTax= numberMinorUnit((String) rules.get(18).getResults(),this.MinorUnitofPublicLightingTax);
        this.MinorUnitofCustomerPayablesInstallment= Integer.parseInt((String) rules.get(19).getResults());
        this.CustomerPayablesInstallment= numberMinorUnit((String) rules.get(20).getResults(),this.MinorUnitofCustomerPayablesInstallment);
        this.MinorUnitofPowerPurchase= Integer.parseInt((String) rules.get(21).getResults());
        this.PowerPurchase= numberMinorUnit((String) rules.get(22).getResults(),this.MinorUnitofPowerPurchase);
        this.MinorUnitofPurchasedKWHUnit= Integer.parseInt((String) rules.get(23).getResults());
        this.PurchasedKWHUnit= Integer.parseInt((String) rules.get(24).getResults());
        this.TokenNumber= (String) rules.get(25).getResults();
        this.DistributionCode = (String) rules.get(26).getResults();
        this.ServiceUnit= Integer.parseInt((String) rules.get(27).getResults());
        this.ServiceUnitPhone= Integer.parseInt((String) rules.get(28).getResults());
        this.MaxKWHLimit= Integer.parseInt((String) rules.get(29).getResults());
        this.TotalRepeat= (String) rules.get(30).getResults();
        this.Message = (String) rules.get(31).getResults();
    }

    public PaymentResponse() {
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @XmlTransient @JsonIgnore
    public String getSwitcherID() {
        return SwitcherID;
    }

    public void setSwitcherID(String switcherID) {
        SwitcherID = switcherID;
    }

    public String getMeterSerialNumber() {
        return MeterSerialNumber;
    }

    public void setMeterSerialNumber(String meterSerialNumber) {
        MeterSerialNumber = meterSerialNumber;
    }

    public String getSubscriberID() {
        return SubscriberID;
    }

    public void setSubscriberID(String subscriberID) {
        SubscriberID = subscriberID;
    }

    @XmlTransient @JsonIgnore
    public String getFlag() {
        return Flag;
    }

    public void setFlag(String flag) {
        Flag = flag;
    }

    public String getPLNReferenceNumber() {
        return PLNReferenceNumber;
    }

    public void setPLNReferenceNumber(String PLNReferenceNumber) {
        this.PLNReferenceNumber = PLNReferenceNumber;
    }

    public String getBukopinReferenceNumber() {
        return BukopinReferenceNumber;
    }

    public void setBukopinReferenceNumber(String bukopinReferenceNumber) {
        BukopinReferenceNumber = bukopinReferenceNumber;
    }

    @XmlTransient @JsonIgnore
    public String getVendingReceiptNumber() {
        return VendingReceiptNumber;
    }

    public void setVendingReceiptNumber(String vendingReceiptNumber) {
        VendingReceiptNumber = vendingReceiptNumber;
    }

    public String getSubscriberName() {
        return SubscriberName;
    }

    public void setSubscriberName(String subscriberName) {
        SubscriberName = subscriberName;
    }

    public String getSubscriberSegmentation() {
        return SubscriberSegmentation;
    }

    public void setSubscriberSegmentation(String subscriberSegmentation) {
        SubscriberSegmentation = subscriberSegmentation;
    }

    public int getPowerConsumingCategory() {
        return PowerConsumingCategory;
    }

    public void setPowerConsumingCategory(int powerConsumingCategory) {
        PowerConsumingCategory = powerConsumingCategory;
    }

    @XmlTransient @JsonIgnore
    public String getBuyingOption() {
        return BuyingOption;
    }

    public void setBuyingOption(String buyingOption) {
        BuyingOption = buyingOption;
    }

    @XmlTransient @JsonIgnore
    public int getMinorUnitofAdminCharge() {
        return MinorUnitofAdminCharge;
    }

    public void setMinorUnitofAdminCharge(int minorUnitofAdminCharge) {
        MinorUnitofAdminCharge = minorUnitofAdminCharge;
    }

    public double getAdminCharge() {
        return AdminCharge;
    }

    public void setAdminCharge(double adminCharge) {
        AdminCharge = adminCharge;
    }

    @XmlTransient @JsonIgnore
    public int getMinorUnitofStampDuty() {
        return MinorUnitofStampDuty;
    }

    public void setMinorUnitofStampDuty(int minorUnitofStampDuty) {
        MinorUnitofStampDuty = minorUnitofStampDuty;
    }

    @XmlTransient @JsonIgnore
    public double getStampDuty() {
        return StampDuty;
    }

    public void setStampDuty(double stampDuty) {
        StampDuty = stampDuty;
    }

    @XmlTransient @JsonIgnore
    public int getMinorUnitofValueAddedTax() {
        return MinorUnitofValueAddedTax;
    }

    public void setMinorUnitofValueAddedTax(int minorUnitofValueAddedTax) {
        MinorUnitofValueAddedTax = minorUnitofValueAddedTax;
    }

    @XmlTransient @JsonIgnore
    public double getValueAddedTax() {
        return ValueAddedTax;
    }

    public void setValueAddedTax(double valueAddedTax) {
        ValueAddedTax = valueAddedTax;
    }

    @XmlTransient @JsonIgnore
    public int getMinorUnitofPublicLightingTax() {
        return MinorUnitofPublicLightingTax;
    }

    public void setMinorUnitofPublicLightingTax(int minorUnitofPublicLightingTax) {
        MinorUnitofPublicLightingTax = minorUnitofPublicLightingTax;
    }

    @XmlTransient @JsonIgnore
    public double getPublicLightingTax() {
        return PublicLightingTax;
    }

    public void setPublicLightingTax(double publicLightingTax) {
        PublicLightingTax = publicLightingTax;
    }

    @XmlTransient @JsonIgnore
    public int getMinorUnitofCustomerPayablesInstallment() {
        return MinorUnitofCustomerPayablesInstallment;
    }

    public void setMinorUnitofCustomerPayablesInstallment(int minorUnitofCustomerPayablesInstallment) {
        MinorUnitofCustomerPayablesInstallment = minorUnitofCustomerPayablesInstallment;
    }

    @XmlTransient @JsonIgnore
    public double getCustomerPayablesInstallment() {
        return CustomerPayablesInstallment;
    }

    public void setCustomerPayablesInstallment(double customerPayablesInstallment) {
        CustomerPayablesInstallment = customerPayablesInstallment;
    }

    @XmlTransient @JsonIgnore
    public int getMinorUnitofPowerPurchase() {
        return MinorUnitofPowerPurchase;
    }

    public void setMinorUnitofPowerPurchase(int minorUnitofPowerPurchase) {
        MinorUnitofPowerPurchase = minorUnitofPowerPurchase;
    }

    public double getPowerPurchase() {
        return PowerPurchase;
    }

    public void setPowerPurchase(double powerPurchase) {
        PowerPurchase = powerPurchase;
    }

    @XmlTransient @JsonIgnore
    public int getMinorUnitofPurchasedKWHUnit() {
        return MinorUnitofPurchasedKWHUnit;
    }

    public void setMinorUnitofPurchasedKWHUnit(int minorUnitofPurchasedKWHUnit) {
        MinorUnitofPurchasedKWHUnit = minorUnitofPurchasedKWHUnit;
    }


    public int getPurchasedKWHUnit() {
        return PurchasedKWHUnit;
    }

    public void setPurchasedKWHUnit(int purchasedKWHUnit) {
        PurchasedKWHUnit = purchasedKWHUnit;
    }

    public String getTokenNumber() {
        return TokenNumber;
    }

    public void setTokenNumber(String tokenNumber) {
        TokenNumber = tokenNumber;
    }

    @XmlTransient @JsonIgnore
    public String getDistributionCode() {
        return DistributionCode;
    }

    public void setDistributionCode(String distributionCode) {
        DistributionCode = distributionCode;
    }

    @XmlTransient @JsonIgnore
    public int getServiceUnit() {
        return ServiceUnit;
    }

    public void setServiceUnit(int serviceUnit) {
        ServiceUnit = serviceUnit;
    }

    @XmlTransient @JsonIgnore
    public int getServiceUnitPhone() {
        return ServiceUnitPhone;
    }

    public void setServiceUnitPhone(int serviceUnitPhone) {
        ServiceUnitPhone = serviceUnitPhone;
    }

    @XmlTransient @JsonIgnore
    public int getMaxKWHLimit() {
        return MaxKWHLimit;
    }

    public void setMaxKWHLimit(int maxKWHLimit) {
        MaxKWHLimit = maxKWHLimit;
    }

    @XmlTransient @JsonIgnore
    public String getTotalRepeat() {
        return TotalRepeat;
    }

    public void setTotalRepeat(String totalRepeat) {
        TotalRepeat = totalRepeat;
    }
}
