package test;

import main.java.com.trimindi.switching.packager.BukopinPackager;
import main.java.com.trimindi.switching.utils.iso.builder.FieldBuilder;
import main.java.com.trimindi.switching.utils.iso.models.IsoField;
import main.java.com.trimindi.switching.utils.iso.models.Rules;
import main.java.com.trimindi.switching.utils.iso.parsing.SDE;
import main.java.com.trimindi.switching.utils.rules.response.ResponseRulesGeneratorPostPaid;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.junit.Test;

import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * Created by sx on 12/05/17.
 */


public class IsoTest {
    private static ISOMsg msg;

    protected static String valueToMinor(double amount, int minor) {
        if (amount % 1 == 0) {
            StringBuilder s = new StringBuilder();
            for (int i = 1; i <= minor; i++) {
                s.append("0");
            }
            return String.valueOf(Integer.toString((int) amount)) + s.toString();
        } else {
            StringBuilder s = new StringBuilder();
            s.append("1");
            for (int i = 1; i <= minor; i++) {
                s.append("0");
            }
            return String.valueOf(Integer.toString((int) (amount * Integer.parseInt(s.toString()))));
        }
    }

    private static String amountGenerator(double amount) {
        String curency = "360";
        String minor = "";
        String harga = "";
        if (amount % 1 == 0) {
            minor = "0";
            harga = String.valueOf(Integer.toString((int) amount));
        } else {
            minor = "2";
            harga = String.valueOf(Integer.toString((int) (amount * 100)));
        }
        return curency + minor + StringUtils.leftPad(harga, 12, "0");
    }

    @Test
    public void testSign() throws NoSuchAlgorithmException {
        String partnerid = "TRY0001";
        String time = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        String password = "TRY0001";
        String combine = partnerid + time + password;
        System.out.println("combine "+ combine);
        String sign = DigestUtils.sha1Hex(combine);
        System.out.println(sign);
    }

    @Test
    public void testIso(){
        msg = new ISOMsg();
        try {
            msg.setMTI("0200");
            msg.setPackager(new BukopinPackager());
            msg.set(12, new SimpleDateFormat("yyyyMMddhhmmss").format(new Date()));
            msg.set(33, "5060000");
            msg.set(40, "001");
            msg.set(41, "0000BMSYARIAHPBB");
            msg.set(48, "0000000");
            System.out.println(new String(msg.pack()));
        } catch (ISOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testResponse(){
        String var = new FieldBuilder.Builder()
                .addValue(new IsoField("AA",100,"_","L"))
                .addValue(new IsoField("ZZ",10,"_","R")).build();
        System.out.println(var);
    }

    @Test
    public void testRepack(){
        //String payload = "00000001100000000001                   00000PERUBAHAN DAYA           2017052220170522123456789012SUBSRIBER NAME           7C2186DF7968BB191D827226E073C24C044121C9B16C85F08F86DD2B74EE91BF000050A276CEB152CEB03FDD4A634AC37E9344A800000000000001520000000005050000020000000005000000020000500000";
//        String payload = "                                    0000000007C2186DF7968BB191D827226E073C24C";
        String payload = "00000005300000000011101044121C9B16CB4A1E3741FDE3924C5E7SUBCRIBER NAME           00005000000000000015R1  0000013000000025002017052205201722052017000000300000C00000000000000000000000000000000000011110000222200000008000000080000000800000008";
        List<Rules> rules = ResponseRulesGeneratorPostPaid.postPaidPaymentResponse(48, true);
        List<Rules> response = new SDE.Builder().setPayload(payload).setRules(rules).generate();
        int a = 0;
        System.out.println(payload.length());
        for(Rules r:rules){
            a += r.getLength();
        }
        System.out.print("size" + a);
        System.out.print(response);

    }

    @Test
    public void testDecimal(){
        double dec = 1000.77;

//        System.out.println(Math.floor(dec));
        System.out.print(valueToMinor(dec,0));

    }

    @Test
    public void testcreate(){
        ISOMsg isoMsg = new ISOMsg();
        try {
            isoMsg.setPackager(new BukopinPackager());
            isoMsg.setMTI("0200");
            isoMsg.set(2,"99504");
            isoMsg.set(4,"102030101030");
            isoMsg.set(11,"102902302010");
            isoMsg.set(12,"12308121231232");
            isoMsg.set(15,"10231023");
            isoMsg.set(26,"1023");
            isoMsg.set(32,"071020203");
            isoMsg.set(33,"071020204");
            isoMsg.set(39,"0000");
            isoMsg.set(41,"0000000000000000");
            isoMsg.set(48,"Binary iso the simplest kind of number system that uses only two digits of 0 and 1. By using these digits computational problems can be solved by machines because in digital electronics a transistor iso used in two states. Those two states can be represented by 0 and 1. That iso why this number system iso the most preferred in modern computer engineer, networking and communication specialists, and other professionals.");
            String pack = new String(isoMsg.pack());
            isoMsg.dump(System.out,"packed MSG");

            System.out.println("pack msg " + pack);

            ISOMsg unpack = new ISOMsg();
            unpack.setPackager(new BukopinPackager());
            unpack.unpack(pack.getBytes());
            unpack.dump(System.out,"unpacked MSG");
            System.out.println("Unpack msg " + new String(unpack.pack()));

        } catch (ISOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void view() throws ISOException {
        ISOMsg msg = new ISOMsg();
        msg.setPackager(new BukopinPackager());
        String s = "2400503000418081010005995013600000000101600100000000001200805020723406015070000000070112222ABC0000000000001233000000053000000000111018A2F3BFDDB7BEEB44914A4A4892D362ASUBSCRIBER NAME 53581022-2501111R1000000450000001600200804200804202008032100000100000D00000000000000000000000000000001745000017550000000000000000000000000000000000372200100000000001200805020723000000000 A4892D362ASUBSCRIBERNAME53581022-2501111R1000000450000001600200804200804202008032100000100000D00000000000000000000000000000001745000017550000000000000000000000000000000000 2D362ASUBSCRIBER NAME53581022-2501111R1000000450000001600200804200804202008032100000100000D000000000000000000000000000000017450000175500000000000000000000000000000000000000000000000000000000 A4892D362ASUBSCRIBERNAME 53581022-2501111 R1 000000450000001600200804200804202008032100000100000D00000000000000000000000000000001745000017550000000000000000000000000000000000";
        msg.unpack(s.getBytes());
        msg.dump(System.out, "==>");
    }

    @Test
    public void parsing() {
        String field48 = "00000005300000000011018A2F3BFDDB7BEEB4491 4A4A4892D362ASUBSCRIBER NAME53581022-2501111R1000000450000001600200804200804202008032100000100000D00000000000000000000000000000001745000017550000000000000000000000000000000000";
        int start = 7;
        int end = 19;
        HashMap<String, String> parsing = new HashMap<String, String>();
        parsing.put("MSSIDN", field48.substring(start, end));
        start = end + 2;
        end = start + 32;
        parsing.put("PLN REF", field48.substring(start, end));
        start = end + 1;
        end = start + 32;
        parsing.put("BUKOPIN REF", field48.substring(start, end));
        start = end + 1;
        end = start + 25;
        parsing.put("SUBSCRIBE NAME", field48.substring(start, end));
        start = end + 1;
        end = start + 4;
        parsing.put("SUBSCRIBE SEGMENT", field48.substring(start, end));
        start = end + 1;
        end = start + 9;
        parsing.put("PCC", field48.substring(start, end));
        start = end + 1;
        end = start + 1;
        parsing.put("MUAC", field48.substring(start, end));
        start = end + 1;
        end = start + 10;
        parsing.put("AC", field48.substring(start, end));
        System.out.println(end);

        System.out.println(parsing);
    }

    public void genStan() {
        Random random = new Random(System.nanoTime());
        int randomInt = random.nextInt(1000000000);
        int random2 = random.nextInt(1000000000);
        String first = String.valueOf(randomInt) + String.valueOf(random2);
        System.out.println(first.substring(0, 12));
    }

    @Test
    public void trystan() {
        for (int i = 0; i < 10000; i++) {
            genStan();
        }
    }

}
