package test;

import main.java.com.trimindi.switching.utils.iso.models.Rules;
import main.java.com.trimindi.switching.utils.iso.parsing.SDE;
import main.java.com.trimindi.switching.utils.rules.response.ResponseRulesGeneratorNonTagList;
import main.java.com.trimindi.switching.utils.rules.response.ResponseRulesGeneratorPostPaid;
import org.junit.Test;

import java.util.List;

/**
 * Created by HP on 18/05/2017.
 */
public class ParsingISONonTagListTest {

    @Test
    public void parsingInquiry(){
        String payload = "00000001100000000001                   00000PERCOBAAN                PERUBAHAN DAYA           2017051820170518123456789012SUBSRIBER NAME           00000000000000000000000000000000360408DD372DF81FCF72FF4149DB8219000055D9DF75D4677BA801EBA09C41F07B25158800000000000001520000000005000000020000000005000000020000500000";
        List<Rules> rules = new SDE.Builder().setRules(ResponseRulesGeneratorNonTagList.nonTagListInquiryResponse(48,true)).setPayload(payload).generate();
        System.out.println(rules);
    }
    @Test
    public void parsingInquiry62(){
        String payload = "02200000000050000000";
        List<Rules> rules = new SDE.Builder().setRules(ResponseRulesGeneratorNonTagList.nonTagListInquiryResponse(62,true)).setPayload(payload).generate();
        System.out.println(rules);
    }
}
