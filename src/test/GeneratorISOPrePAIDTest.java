package test;

import main.java.com.trimindi.switching.utils.generator.PrePaidGenerator;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.junit.Test;

/**
 * Created by sx on 13/05/17.
 */


public class GeneratorISOPrePAIDTest {
    @Test
    public void generateNetworkSignOn() throws ISOException {
        ISOMsg msg = PrePaidGenerator.generateNetworkSignOn();
        msg.dump(System.out,"Network Sign-ON");
        System.out.println(new String(msg.pack()));
    }
    @Test
    public void generateNetworkSignOff() throws ISOException {
        ISOMsg msg = PrePaidGenerator.generateNetworkSignOFF();
        msg.dump(System.out,"Network Sign-OFF");
        System.out.println(new String(msg.pack()));

    }
    @Test
    public void generateNetworkEchoTest() throws ISOException {
        ISOMsg msg = PrePaidGenerator.generateNetworkEchoTest();
        msg.dump(System.out,"Network Echo Test");
        System.out.println(new String(msg.pack()));

    }


    @Test
    public void generateInqury() throws ISOException {
        ISOMsg msg = PrePaidGenerator.generateInquiry("5401","102030129301");
        msg.dump(System.out,"Inquiry");
        System.out.println(new String(msg.pack()));

    }

    @Test
    public void generatePayment() throws ISOException {
//        ISOMsg msg = PrePaidGenerator.generatePurchase("5401", "102030129301", "20000");
//        msg.dump(System.out, "Payment");
//        System.out.println(new String(msg.pack()));
    }
    @Test
    public void generatePaymentAdvice() throws ISOException {
//        ISOMsg msg = PrePaidGenerator.generatePurchaseAdvice("5401", "102030129301", "10001");
//        msg.dump(System.out, "Payment Advice");
//        System.out.println(new String(msg.pack()));
    }
}

